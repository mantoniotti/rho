;;;; -*- Mode: Lisp -*-

;;;; rho-package.lisp --

;;;; See the file COPYING for licencing and copyright information.

(defpackage "IT.UNIMIB.DISCO.MA.RHO" (:use "CL")
  (:nicknames "RHO")

  (:shadow cl:array cl:array-element-type #| ... and auxiliary functions ... |#)
  (:shadow cl:vector #| ... and auxiliary functions ... |#)
  
  (:import-from "PROTOTYPES" "PROTOTYPE-ELEMENT-OF")

  (:export "ARRAY")
  (:export "VECTOR")
  (:export "MATRIX"
   "NCOL"
   "NROW"
   )

  (:export
   "MAKE-DATA-FRAME"
   ;; Add other...
   )

  (:export
   "MAKE-STRAND"
   ;; Add other...
   )

  (:export
   "DATA-FRAME-COLUMN-NAMES"
   "DATA-FRAME-COLUMN-TYPES"
   "DATA-FRAME-AS-MATRIX"
   )

  (:export
   "REF$"
   "AT"
   "READER"
   "WRITER"
   )
  )

;;;; end of file -- rho-package.lisp --
