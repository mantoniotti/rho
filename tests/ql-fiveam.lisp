;;;; -*- Mode: Lisp -*-

;;;; ql-fiveam.lisp

(in-package "RHO")

(eval-when (:load-toplevel :compile-toplevel :execute)
  (unless (find-package "QL")
    (error "Package Quicklisp is necessary to load the RHO test suite."))
  (ql:quickload "fiveam"))

;;;; end of file -- ql-fiveam.lisp
