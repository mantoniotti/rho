;;;; -*- Mode: Lisp -*-

;;;; tests.lisp

(in-package "RHO")

(eval-when (:load-toplevel :compile-toplevel :execute)
  (shadowing-import '(fiveam:test))
  (use-package "FIVEAM"))

(def-suite rho-testing
           :description "The main RHO test suite")


(def-suite basic-objects-tests
           :in rho-testing
           :description "Basic Object Tests")

(in-suite basic-objects-tests)

(test vector-creation
  (is (vector-p (vector)))
  (is (cl:vectorp (vector-data (vector))))
  (is (equalp #() (vector-data (vector))))
  (is (equalp (vector 1 2 3 4) (vector 1 2 3 4)))
  (is (equalp #(1 2 3 4) (vector-data (vector 1 2 3 4))))
  (is (equalp #(1 2 3 4) (vector-data (vector* (list 1 2 3 4)))))
  (is (equalp #(1 2 3 4) (vector-data (vector* (list 1 2 3 4) :size 4))))
  (is (equalp #(1 2 3 4 1 2) (vector-data (vector* (list 1 2 3 4) :size 6))))
  )


(test array-creation
  (is (array-p (array (list 0))))
  (is (cl:arrayp (array-data (array (list 0)))))

  ;; (skip "Change behavior with NIL initial element.")
  (is (equalp #0ANIL (array-data (array nil :splice nil))))
  (is (equalp #(NIL) (array-data (array nil :splice t))))

  (is (equalp (array 42 :dim 10) (array 42 :dim (list 10))))
  (is (equalp #(42 42 42) (array-data (array 42 :dim 3))))
  (is (equalp #2A((42 42) (42 42)) (array-data (array 42 :dim '(2 2)))))
  (is (equalp #2A((42) (42) (42) (42)) (array-data (array 42 :dim '(4 1)))))

  (is (equalp #2A((42) (42) (42) (42))
              (array-data (array #(42 42 42 42) :dim '(4 1) :splice nil))))
  (is (equalp #2A((42) (42) (42) (42) (42))
              (array-data (array #(42 42 42 42) :dim '(5 1) :splice nil))))
  (is (equalp #2A((42) (42) (42) (42) (nil))
              (array-data (array #(42 42 42 42)
                                 :dim '(5 1)
                                 :splice nil
                                 :fill nil))))
  (is (equalp #2A((#(1 2 3)) (#(1 2 3)))
              (array-data (array #(1 2 3) :dim '(2 1) :splice t))))
  )


(test matrix-creation
  (is (array-p (matrix 42 :nrow 4 :ncol 1)))
  (is (cl:arrayp (matrix-data (matrix 42 :nrow 4 :ncol 1))))
  (is (equalp #2A((42) (42) (42) (42)) (matrix-data (matrix 42 :nrow 4))))
  (is (equalp #2A((42 42 42 42)) (matrix-data (matrix 42 :ncol 4))))
  (is (equalp #2A((42 42 42 42)) (matrix-data (matrix 42 :ncol 4 :splice nil))))
  (is (equalp #2A((42 42 42 42)) (matrix-data (matrix 42 :ncol 4 :splice t))))
  (is (equalp #2A((42 42 42 42)) (matrix-data (matrix #(42 42 42 42) :ncol 4))))
  (is (equalp #2A((1 2 3)) (matrix-data (matrix #(1 2 3 4 5) :ncol 3))))
  (is (equalp #2A((1 2 3 4 5 1)) (matrix-data (matrix #(1 2 3 4 5) :ncol 6))))
  (is (equalp #2A((1 2 3 4 5 NIL)) (matrix-data (matrix #(1 2 3 4 5) :ncol 6 :fill nil))))
  (is (equalp #2A((#(42 42 42 42) #(42 42 42 42)))
              (matrix-data (matrix #(42 42 42 42) :ncol 2 :splice t))))

  (is (equalp #2A((1 0) (0 1)) (matrix-data (id 2))))
  )


(test range-creation
  (is (range-p (range)))
  (is (range-p (range 1 10)))
  (is (vector-p (range 1 10)))
  (is (cl:vectorp (vector-data (range 1 10))))
  (is (= 9 (cl:length (vector-data (range 1 10)))))
  (is (= 5 (cl:length (vector-data (range 1 10 2)))))
  (is (equalp #(1 2 3 4 5 6 7 8 9)
              (vector-data (range 1 10))))
  )


(def-suite indexing-tests
           :in rho-testing
           :description "Indexing Tests")

(def-fixture with-containers (&key a a3 v)
  (declare (ignorable a a3 v))
  ;; (break "Sun chi!")
  (format t "(WITH-CONTAINER fixture) ")
  (&body))


(in-suite indexing-tests)

(test (simple-array-indexing
       :fixture (with-containers :a (array (range 1 12) :dim (list 3 4))
                                 :a3 (array (range 1 12) :dim (list 3 2 2))
                                 :v (array (range 1 12) :dim (list 3 2 2))
                                 )
       )
  (is (= 1 (at (vector 1 2 3 4) 0)))
  (is (= 4 (at (vector 1 2 3 4) 3)))
  (is (char= #\a (at "asdf" 0)))
  (is (= 1 (at a 0 0)))
  (is (= 1 (at v 0 0 0)))
  (is (= 4 (at a 0 3)))
  (is (= 4 (at v 0 1 1)))


  ;; Accessses via ROW-MAJOR-AREFs and single vector index.
  
  (is (equalp #(2 3 9) (array-data (at a #(1 2 8)))))
  (is (equalp #(2 3 9) (array-data (at a (vector 1 2 8)))))
  )


(test (slice-array-indexing
       :fixture (with-containers :a (array (range 1 12) :dim (list 3 4))
                                 :a3 (array (range 1 12) :dim (list 3 2 2))
                                 :v (array (range 1 12) :dim (list 3 2 2))
                                 )
       )

  (is (equalp #(2 1) (array-data (at a3 0 0 #(1 0)))))
  (is (equalp #2A((9) (11)) (array-data (at a3 2 '* 0))))
  (is (equalp #2A((11) (9)) (array-data (at a3 2 #(1 0) 0))))
  (is (equalp #2A((7) (3)) (array-data (at a3 #(1 0) 1 0))))
  (is (equalp #3A(((3) (1)) ((7) (5)) ((11) (9)))
              (array-data (at a3 '* #(1 0) 0))))
  )


(test (atomic-assignments
       :fixture (with-containers :a (array 42 :dim 5)
                                 :v (vector* 42 :size 5)
                                 :a3 (array (loop for i from 1 upto 9
                                                  collect i)
                                            :dim (list 3 3)
                                            :element-type t)
                                 )
       ;; :depends-on (and basic-objects-tests)
       )
      
  ;; (print a3)
  (finishes (setf (at v 0) 123
                  (at v 4) 444
                  ))
  (is (= 123 (at v 0)))
  (is (= 444 (at v 4)))

      
  (finishes (setf (at a3 1 1) #(42 42)))
  (is (equalp #(42 42) (at a3 1 1)))
  )


(test (slice-assignments
       :fixture (with-containers :a (array 42 :dim 5)
                                 :v (vector* 42 :size 5)
                                 :a3 (array (loop for i from 1 upto 9
                                                  collect i)
                                            :dim (list 3 3)
                                            :element-type t)
                                 )
       ;; :depends-on (and basic-objects-tests)
       )
      
  ;; (print a3)
  (signals error (setf (at v #(1 3)) #(-1 -2)))
  (is-false (= -1 (at v 1)))
  (is-false (= -2 (at v 3)))

  (finishes (setf (at a #(1 3)) #(-1 -2)))
  (is (= -1 (at a 1)))
  (is (= -2 (at a 3)))
  )


(def-suite attribute-tests
           :in rho-testing
           :description "Attribute Tests")

(def-suite names-and-dimnames-tests
           :in attribute-tests
           :description "Tests about 'names' and 'dimnames' for arrays and vectors.")

(in-suite attribute-tests)

(in-suite names-and-dimnames-tests)

(test (array-vectors-names
       :fixture (with-containers :a (array (loop for i from 1 upto 9
                                                 collect i)
                                           :dim (list 3 3)
                                           :dimnames (list nil '("qq" "ww" "ee")))
                                 :v (vector* (loop for i from 1 upto 5
                                                   collect i)
                                             :size 5
                                             :names (list "a" "s" "d" "f" "g")))
       )
  (finishes (setf (names a) (list "a" "s" "d" "f" "g")))
  (is (equalp (list "a" "s" "d" "f" "g") (names v)))
  (is (equalp (list "a" "s" "d" "f" "g") (names a)))
  (is (equalp (list () (list "qq" "ww" "ee")) (dimension-names a)))
  (is (equalp (list :scalar (list "qq" "ww" "ee"))
              (at-follow-dimnames 1 a (dimension-names a) () 0 (list '*))))
  (is (equalp (list :scalar (list "ee" "qq"))
              (at-follow-dimnames 1 a (dimension-names a) () 0 (list (cl:vector 2 0)))))
  (is (equalp (list "ee" "qq")
              (names (at a 0 (list "ee" "qq")))))
  (is (equalp (list nil (list "ee" "qq"))
              (dimension-names (at a (list 1 0) (list "ee" "qq")))))
  )

;;;; end of file -- tests.lisp
