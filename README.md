RHO
====
Marco Antoniotti  
See file COPYING for licensing information


DESCRIPTION
-----------

The ***&rho;*** (RHO) Common Lisp library contains a set of data structures and
functions to provide some of the functionalities of **[R](https://www.r-project.org/)**.

I blogged about it in [(within parens...)](https://within-parens.blogspot.com/2016/12/rcreational-common-lisp.html)
some time ago. The library provides most indexing trickery that you
can find in [R](https://www.r-project.org/), although in a Lispy way
(and indexed at 0).  It was fun to write.


A NOTE ON FORKING
-----------------

Of course you are free to fork the project subject to the current
licensing scheme.  However, before you do so, I ask you to consider
plain old "cooperation" by asking me to become a developer.
It helps keeping the entropy level at an acceptable level.


Enjoy.
