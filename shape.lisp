;;;; -*- Mode: Lisp -*-

;;;; shape.lisp --
;;;; The main function used to (re)shape a sequence or 'N-grid' data
;;;; structure.

(in-package "RHO")


;;;; Prologue
;;;; ========

;;; sos-dimensions

(defun sos-dimensions (sos)
  "'Dimensions' of a 'sequence of sequences'."
  (let ((ed (dimensions sos)))
    (when ed
      ;; A structured SoS.
      (let ((ss0-dims (sos-dimensions (elt sos 0))))
        (unless (every #'(lambda (e) (equal e ss0-dims))
                       (map 'list #'sos-dimensions sos))
          (error "Subsequences of ~S do not have the same structure."
                 sos
                 ss0-dims))
        (cons (length sos) ss0-dims))
      )))


;;; dimensions

(defgeneric dimensions (data &optional recursive)
  (:method ((data t) &optional recursive)
   (declare (ignore recursive))
   nil)

  (:method ((data null) &optional recursive)
   (declare (ignore recursive))
   nil)

  (:method ((data cl:list) &optional recursive)
   (if recursive
       (sos-dimensions data)
       (list (list-length data))))

  (:method ((data cl:vector) &optional recursive)
   (if recursive
       (sos-dimensions data)
       (list (length data))))

  (:method ((data cl:array) &optional recursive)
   (declare (ignore recursive))
   (array-dimensions data))
  )


;;; is-scalar

(defgeneric is-scalar (x)
  (:method ((x t)) t)
  (:method ((x cl:list)) nil)
  (:method ((x cl:cons)) nil)
  (:method ((x cl:array)) nil)

  ;; The following must be defined for each class/structure.

  ;; (:method ((x array)) nil)
  ;; (:method ((x vector)) nil)
  )


;;; rank

(defgeneric rank (data &optional recursive)
  (:method ((data t) &optional recursive)
   (declare (ignore recursive))
   0)

  (:method ((data cl:list) &optional recursive)
   (if recursive
       (list-length (sos-dimensions data))
       1
       ))

  (:method ((data cl:vector) &optional recursive)
   (if recursive
       (list-length (sos-dimensions data))
       1))

  (:method ((data cl:array) &optional recursive)
   (declare (ignore recursive))
   (cl:array-rank data))
  )


;;; shape
;;; This is actually more Matlab.

(defgeneric shape (result-type data dimensions
                               &key
                               by-row
                               element-type
                               check-total-size
                               fill
                               &allow-other-keys)
  (:documentation
   "Shapes the DATA into a RESULT-TYPE having a given set of DIMENSIONS.")
  )


;;;; Implementation
;;;; --------------

;;; flatten

(defun flatten (data &key (copy t) (element-type t etsp))
  (declare (type (or cl:array list) data)
           (type boolean copy))
  (etypecase data
    (cl:array (let ((da (make-array (array-total-size data)
                                 :element-type (if etsp
                                                   element-type
                                                   (cl:array-element-type data))
                                 :displaced-to data)))
             (if copy
                 (copy-seq da)
                 da)))

    (list (labels ((fl (l)
                     (cond ((endp l) ())
                           ((atom (first l))
                            (cons (first l) (fl (rest l))))
                           (t ; (CONSP (first l))
                            (append (fl (first l))
                                    (fl (rest l))))))
                   )
            (fl data)))
    ))


;;; incompatible-dimensions

(define-condition incompatible-dimensions (error)
  ((dims1 :reader incompatible-dimensions-1
          :initarg :ds1
          :initform '*
          )
   (dims2 :reader incompatible-dimensions-2
          :initarg :ds2
          :initform '*
          )
   (context :reader incompatible-dimensions-context
            :initarg :context)
   )
  (:report (lambda (idc stream)
             (format stream "Incompatible dimensions ~S and ~S."
                     (incompatible-dimensions-1 idc)
                     (incompatible-dimensions-2 idc))))
  )


;;; shape methods

(defmethod shape ((result-type (eql 'cl:vector))
                  (v cl:vector)
                  dimensions
                  &key
                  (by-row t)
                  (element-type (cl:array-element-type v) etsp)
                  (check-total-size t)
                  (fill t)
                  &allow-other-keys
                  )
  (declare (ignore by-row))
  (let* ((dimensions (etypecase dimensions
                       (fixnum (list dimensions))
                       (sequence (coerce dimensions 'list))))
         (rank (list-length dimensions))
         (vl (length v))
         )
    (declare (type list dimensions)
             (type fixnum rank vl))

    (unless (= 1 rank)
      (error "DIMENSIONS must be of length 1 when shaping a cl:vector ~
              into a cl:vector; it is ~S."
             dimensions))

    (let* ((d (first dimensions))
           (eqds (= d vl))
           )
      (when (and check-total-size
                 (integerp d)
                 (not eqds))
        (error 'incompatible-dimensions
               :ds1 (list vl)
               :ds2 (list d)))
      (if eqds
          (make-array d
                      :initial-contents v
                      :element-type element-type)
          ;; In this case CHECK-TOTAL-SIZE must have been false.
          (let ((r (make-array d
                               :element-type element-type
                               :initial-element (prototype-element-of element-type))))
            (if fill
                (dotimes (i d r)
                  (setf (aref r i) (aref v (mod i vl))))
                (dotimes (i (min d vl) r)
                  (setf (aref r i) (aref v i))))))
      )))


(defmethod shape ((result-type (eql 'cl:vector))
                  (l list)
                  dimensions
                  &key
                  (by-row t)
                  (element-type t etsp)
                  (check-total-size t)
                  (fill t)
                  &allow-other-keys
                  )
  (declare (ignore by-row))
  (let* ((dimensions (etypecase dimensions
                       (fixnum (list dimensions))
                       (sequence (coerce dimensions 'list))))
         (rank (list-length dimensions))
         (ll (list-length l))
         )
    (declare (type list dimensions)
             (type fixnum rank ll))

    (unless (= 1 rank)
      (error "DIMENSIONS must be of length 1 when shaping a list ~
              into a cl:vector; it is ~S."
             dimensions))

    (let* ((d (first dimensions))
           (eqds (= d ll))
           )
      (when (and check-total-size
                 (integerp d)
                 (not eqds))
        (error 'incompatible-dimensions
               :ds1 (list ll)
               :ds2 (list d)))
      (if eqds
          (make-array d
                      :initial-contents l
                      :element-type element-type)
          ;; In this case CHECK-TOTAL-SIZE must have been false.
          (let ((r (make-array d
                               :element-type element-type
                               :initial-element (prototype-element-of element-type))))
            (if fill
                (dotimes (i d r)
                  (setf (aref r i) (nth (mod i ll) l)))
                (dotimes (i (min d ll) r)
                  (setf (aref r i) (nth i l))))))
      )))


(defmethod shape ((result-type (eql 'list))
                  (s sequence)
                  dimensions
                  &key
                  (by-row t)
                  (element-type (if (cl:vectorp s)
                                    (cl:array-element-type s)
                                    t)
                                etsp)
                  (check-total-size t)
                  (fill t)
                  &allow-other-keys)
  (coerce (shape 'cl:vector s dimensions
                 :by-row by-row
                 :element-type element-type
                 :check-total-size check-total-size
                 :fill fill)
          'list)
  )


(defmethod shape ((result-type (eql 'list))
                  (a cl:array)
                  dimensions
                  &key
                  (by-row t)
                  (element-type (cl:array-element-type a) etsp)
                  (check-total-size t)
                  (fill t)
                  &allow-other-keys)
  (coerce (shape 'cl:vector a dimensions
                 :by-row by-row
                 :element-type element-type
                 :check-total-size check-total-size
                 :fill fill
                 )
          'list)
  )


(defmethod shape ((result-type (eql 'cl:vector))
                  (a cl:array)
                  dimensions
                  &key
                  (by-row t)
                  (element-type (cl:array-element-type a) etsp)
                  (check-total-size t)
                  (fill t)
                  &allow-other-keys
                  )
  (declare (ignore by-row))
  (let* ((dimensions (etypecase dimensions
                       (fixnum (list dimensions))
                       (sequence (coerce dimensions 'list))))
         (rank (list-length dimensions))
         (ats (array-total-size a))
         )
    (declare (type list dimensions)
             (type fixnum rank ats))

    (unless (= 1 rank)
      (error "DIMENSIONS must be of length 1 when shaping an cl:array ~
              into a cl:vector; it is ~S."
             dimensions))

    (let* ((d (first dimensions))
           (eqds (= d ats))
           )
      (when (and check-total-size
                 (integerp d)
                 (not eqds))
        (error 'incompatible-dimensions
               :ds1 (array-dimensions a)
               :ds2 (list d)))
      (if eqds
          (let ((r (make-array d
                               :displaced-to a
                               :element-type element-type)))
            (declare (dynamic-extent r))
            ;; Should be faster.
            (copy-seq r))
          
          ;; In this case CHECK-TOTAL-SIZE must have been false.
          (let ((r (make-array d
                               :element-type element-type
                               :initial-element (prototype-element-of element-type))))
            (if fill
                (dotimes (i d r)
                  (setf (aref r i) (row-major-aref a (mod i ats))))
                (dotimes (i (min d ats) r)
                  (setf (aref r i) (row-major-aref a i)))))
          ))))


(defmethod shape ((result-type (eql 'cl:array))
                  (a cl:array)
                  dimensions
                  &key
                  (by-row t)
                  (element-type (cl:array-element-type a) etsp)
                  (check-total-size t)
                  (fill t)
                  &allow-other-keys
                  )
  (declare (ignore by-row))
  (let* ((dimensions (etypecase dimensions
                       (fixnum (list dimensions))
                       (sequence (coerce dimensions 'list))))
         (d (reduce '* dimensions))
         (ats (array-total-size a))
         (eqds (= d ats))
         )
    (declare (type list dimensions)
             (type fixnum d ats)
             (type boolean eqds))

    (when (and check-total-size (not eqds))
      (error 'incompatible-dimensions
             :ds1 (array-dimensions a)
             :ds2 dimensions))

    ;; No special case for equal size arrays.
    (let ((r (make-array dimensions
                         :element-type element-type
                         :initial-element (prototype-element-of element-type))))
      (if fill
          (dotimes (i d r)
            (setf (row-major-aref r i) (row-major-aref a (mod i ats))))
          (dotimes (i (min d ats) r)
            (setf (row-major-aref r i) (row-major-aref a i)))))
    ))


(defmethod shape ((result-type (eql 'cl:array))
                  (l list)
                  dimensions
                  &key
                  (by-row t)
                  (element-type t etsp)
                  (check-total-size t)
                  (fill t)
                  &allow-other-keys
                  )
  (declare (ignore by-row))
  (let* ((dimensions (etypecase dimensions
                       (fixnum (list dimensions))
                       (sequence (coerce dimensions 'list))))
         (d (reduce #'* dimensions))
         (lol-dims (dimensions l t))
         (dl (reduce #'* lol-dims))
         (eqds (= d dl))
         )
    (declare (type list dimensions lol-dims)
             (type fixnum d dl)
             (type boolean eqds))

    (when (and check-total-size (not eqds))
      (error 'incompatible-dimensions
             :ds1 lol-dims
             :ds2 dimensions))

    ;; No special case for equal size arrays.
    (let ((ta (make-array lol-dims
                          :element-type element-type
                          :initial-contents l))
          (r (make-array dimensions
                         :element-type element-type
                         :initial-element (prototype-element-of element-type)))
          )
      (if fill
          (dotimes (i d r)
            (setf (row-major-aref r i) (row-major-aref ta (mod i dl))))
          (dotimes (i (min d dl) r)
            (setf (row-major-aref r i) (row-major-aref ta i)))))
    ))


(defmethod shape ((result-type (eql 'cl:array))
                  (v cl:vector)
                  dimensions
                  &key
                  (by-row t)
                  (element-type t etsp)
                  (check-total-size t)
                  (fill t)
                  &allow-other-keys
                  )
  (declare (ignore by-row))
  (let* ((dimensions (etypecase dimensions
                       (fixnum (list dimensions))
                       (sequence (coerce dimensions 'list))))
         (d (reduce #'* dimensions))
         (dl (cl:length v))
         (eqds (= d dl))
         )
    (declare (type list dimensions)
             (type fixnum d dl)
             (type boolean eqds))

    (when (and check-total-size (not eqds))
      (error 'incompatible-dimensions
             :ds1 (list dl)
             :ds2 dimensions))

    (let ((r (make-array dimensions
                         :element-type element-type
                         :initial-element (prototype-element-of element-type)))
          )
      (if fill
          (dotimes (i d r)
            (setf (row-major-aref r i) (aref v (mod i dl))))
          (dotimes (i (min d dl) r)
            (setf (row-major-aref r i) (aref v i)))))
    ))


;;; Cute special case.  Hope not too many demons will fly out of my
;;; nose.

(defmethod shape ((result-type (eql 'cl:array))
                  e
                  dimensions
                  &key
                  (by-row t)
                  (element-type (type-of e))
                  (check-total-size t)
                  (fill t)
                  &allow-other-keys)
  (declare (ignore check-total-size by-row))

  (assert (atom e))

  (let ((dimensions (etypecase dimensions
                      (fixnum (list dimensions))
                      (sequence (coerce dimensions 'list))))
        )
    (declare (type list dimensions))

    (cond ((and dimensions fill)
           (make-array dimensions
                       :element-type element-type
                       :initial-element e))
          (dimensions
           (let ((r (make-array dimensions
                                :element-type element-type
                                :initial-element (prototype-element-of element-type)))
                 )
             (setf (row-major-aref r 0) e)
             r))

          (t ; Does not matter what FILL is here...
           (make-array nil
                       :element-type element-type
                       :initial-element e))
          )))


(defmethod shape ((result-type (eql 'cl:vector))
                  e
                  dimensions
                  &key
                  (by-row t)
                  (element-type (type-of e))
                  (check-total-size t)
                  (fill t)
                  &allow-other-keys)
  (shape 'cl:array e dimensions
         :by-row by-row
         :element-type element-type
         :check-total-size check-total-size
         :fill fill))


;;;; end of file -- shape.lisp
