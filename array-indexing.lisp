;;;; -*- Mode: Lisp -*-

;;;; array-indexing.lisp --
;;;; Array (and matrices and vectors) indexing and assignments are
;;;; collected here.

;;;; Notes:
;;;;
;;;; 20161212 MA:
;;;; Decision to change SETF-AT-FC-ACCESS to keep track of "value"
;;;; indexes (which applies mostly to ARRAYs)
;;;;
;;;; 20161202 MA:
;;;; Possibly fixed the latest glitches.
;;;;
;;;; 20161201 MA:
;;;; Things seem to work following names.  All NREVERSE have been
;;;; changed to REVERSE because there no prrof (yet) that the code
;;;; does not share list structures (and in one occasion it actually
;;;; broke down).
;;;;
;;;; 20161122 MA:
;;;; Started working on AT-FOLLOW-DIMNAMES
;;;; Also: it would be probably better to rework the internal data
;;;; structures for arrays and just keep everything linear while
;;;; accessing stuff using ROW-MAJOR-AREF and ARRAY-ROW-MAJOR-INDEX.
;;;; At a minimum, could possibly rework AT-FC-ACCESS using
;;;; ARRAY-ROW-MAJOR-INDEX.
;;;; 
;;;; 20160818 MA:
;;;; AT-FC-ACCESS for arrays works nicely.  Now we follow the names.
;;;;
;;;; 20160805 MA:
;;;; Decision taken to keep index following and names following separate.
;;;;
;;;; 20160331 MA:
;;;; Moved all the code here from the other files. This
;;;; file should prbably be renamed 'indexing'.


(in-package "RHO")


;;;; Prologue
;;;; ========

#|

The code is organized around two main generic functions that create a
'getter' and a 'setter' function that eventually get called to perform
what AT and (SETF AT) need to do.  The two generic functions are
AT-FC-ACCESS and SETF-AT-FC-ACCESS.

The methods for these functions are written mostly in the same way:

They first check whether they are at the last 'index', i.e., the
remaining indexes list is empty, and if so, they create the actual
'getter' or 'setter' function.  Otherwise, they recur.

In both cases, base and recursive step, when the current index is a
VECTOR (or a CL:VECTOR, but not a STRING) there are a number of
sub-cases to be considered:

1 - FIXNUM vector
2 - REAL vector
3 - 'name' vector
4 - BOOLEAN vector

The dispatching on the different vector contents must be done by hand
and the code reflects this breakdown.

|#


;;;; Common variables
;;;; ----------------

;;; *drop*

(defvar *drop* nil
  "If non NIL the indexing methods will drop names and dimnames.")


;;;; Useful functions and macros
;;;; ---------------------------

;;; dropping-names

(defmacro dropping-names (&body forms)
  "Executes FORMS in an environment where the special variable *DROP*
is set to T."
  `(let ((*drop* t))
     (declare (special *drop*))
     ,@forms))


;;; is-scalar-index

(defun is-scalar-index (i)
  (or (stringp i)
      (and (is-scalar i)
           (not (and (symbolp i)
                     (string= "*" i)))
           )))


;;; all-scalars-p

(defun all-indexes-are-scalars-p (seq)
  (declare (type sequence seq))
  (every #'is-scalar-index seq))


;;; collapse
;;; --------

(defun collapse (l &key (test #'eql))
  "Collapse repetitions in list L according to TEST.

Examples:

cl-prompt> (collapse '(2 1 2 3 2 2 4 4 1 1 1 6 2)
                     :test (lambda (x y) (= x y 1)))
(2 1 2 3 2 2 4 4 1 6 2)
"
  (declare (type cl:list l)
           (type (function (t t) bolean) test))
  (if (null (rest l))
      l
      (destructuring-bind (x y &rest r)
          l
        (if (funcall test x y)
            (collapse r :test test)
            (cons x (collapse r :test test))))))


;;; Extractors
;;; ----------
;;; These functions are used for single vector indices.

;;; extract-fixunm-indexed-elements
;;; Original function.

(defun extract-fixunm-indexed-elements (vec v index i)
  (declare (ignore vec index)
           (type array vec)
           (type cl:vector v index)
           (type (cl:vector fixnum) i))
  (let* ((vl (cl:length v))
         (il (cl:length i))
         )
    (declare (type fixnum vl il))
  
    (if (every #'minusp i)
        (loop with i = (sort (copy-seq i) #'>)
              with neg-i of-type fixnum = 0
              with neg-i-e of-type fixnum = (- (aref i neg-i))
              for idx of-type fixnum from 0 below vl
              if (= idx neg-i-e) do
              (incf neg-i)
              (when (> neg-i (1- il))
                (loop-finish))
              (setf neg-i-e (- (aref i neg-i)))
              else
              collect (aref v idx) into result
              finally
              (return
               (apply #'vector
                      (nconc result
                             (and (< idx vl)
                                  (coerce (subseq v (1+ idx)) 'list))))))
        (loop for idx of-type fixnum across i
              for e = (aref v idx)
              collect e into result
              finally (return (apply #'vector result)))
        )))


;;; extract-boolean-indexed-elements

(defun extract-boolean-indexed-elements (vec v index i)
  (declare (ignore vec index)
           (type cl:vector v)
           (type (cl:vector boolean) i))
  (let* ((vl (cl:length v))
         (il (cl:length i))
         )
    (declare (type fixnum vl il))
    (loop for idx of-type boolean across i
          for e across v
          for count of-type fixnum from 0
          when idx collect e into selected
          when (>= count il) do (loop-finish)
          finally
          (return
           (if (< count vl)
               (let ((final-selection
                      (nconc selected
                             (coerce (subseq v (1+ count)) 'list)))
                     )
                 (apply #'vector final-selection))
               (apply #'vector selected))
           ))))


;;; extract-boolean-indexed-elements

(defun extract-name-indexed-elements (vec v index i)
  (declare (ignore index)
           (type cl:vector v i))
  (let ((el-names (vector-names vec))
        (el-names-pos (vector-names-pos vec))
        )
    (declare (type cl:sequence el-names)
             (type (or null hash-table) el-names-pos))

    (flet ((extract-named-elements (el-names-pos)
             (declare (type hash-table el-names-pos))
             (loop for name across i
                   collect (multiple-value-bind (pos pos-found)
                               (gethash name el-names-pos)
                             (when pos-found
                               ;; When not found, decide
                               ;; later how to represent NA.
                               (aref v pos)))
                   into result
                   finally
                   (return (vector* :names i
                                    :initial-contents result)))
             )
           )
      (cond ((and el-names el-names-pos)
             (extract-named-elements el-names-pos)
             )
            (el-names
             (extract-named-elements 
              (setf (vector-names-pos vec)
                    (init-names el-names)))
             )
            (t
             (apply #'vector (make-list (length i))))
            ))
    ))


;;;; Indexing
;;;; ========

;;; Auxiliary Functions and Data Structures for AT
;;; ----------------------------------------------

;;; ensure-fixnum-indices
;;; ---------------------

(defun ensure-fixnum-indices (a idxs)
  (declare (type array a)
           (type cl:list idxs))
  ;; (assert (all-scalars-p idxs))
  (loop for i in idxs
        for axis of-type fixnum from 0
        if (is-scalar-index i)
        collect (etypecase i
                  (fixnum (if (minusp i)
                              (return-from ensure-fixnum-indices nil)
                              i))
                  (real (if (minusp i)
                            (return-from ensure-fixnum-indices nil)
                            (truncate i)))
                  ((or string symbol)
                   (let* ((adnp-axis (array-dimension-names-positions a axis))
                          (ni (and adnp-axis (gethash i adnp-axis)))
                          )
                     (unless ni
                       (error "Unknown named index ~S on axis ~s." i axis))
                     ni))
                  )
        else return nil
        end)
  )


;;; get-array-element
;;; ------------------

(defun get-array-element (adata indexes)
  (declare (type cl:list indexes)
           (type cl:array adata))

  #| Originating code:

  (apply #'aref adata (reverse (cons i idxs)))
  |#

  (apply #'aref adata (reverse indexes))
  )


(declaim (inline get-array-element))


;;; tuple-sequence-number
;;; ---------------------
;;; Key function that is used to find the index in a vector given a
;;; set of indexes in an array.

(declaim (ftype (function (sequence sequence) fixnum)
                tuple-sequence-number))

(defun tuple-sequence-number (dims tuple)
  (declare (type sequence dims tuple))
  (loop with res of-type fixnum = 0
        with pos of-type fixnum = 1
        for i downfrom (1- (length dims)) to 0
        do (incf res (* pos (elt tuple i)))
        do (setf pos (* pos (elt dims i)))
        finally (return res))
  )


(defun test-tuple-sequence-number (dims)
  (if (null dims)
      ()
      (loop for i from 0 below (first dims)
            collect (mapcar (lambda (r) (cons i r))
                            (test-tuple-sequence-number (rest dims))))))
            
                            
                                              


;;; at-result, at-scalar-result
;;; ---------------------------
;;; Data structures wrapping results of accesses.
;;;
;;; Notes:
;;; 20161226 MA: Unused.

(defstruct (at-result
            (:constructor at-result (&optional data)))
  (data nil :read-only t))


(defstruct (at-as-scalar-result
            (:include at-result)
            (:constructor at-as-scalar-result (&optional data))))


;;; at-fc-access
;;; ------------
;;; A helper function which returns an 'accessor function' in an
;;; almost continuation passing style.

(defgeneric at-fc-access (index
                          shaped-struct
                          axis
                          previous-indexes
                          more-indexes
                          )
  (:documentation "Creates a function that accesses elements in a SHAPED-STRUCT.

The methods of this generic function build up a lambda in an almost
continuation passing style; during the execution the following
invariants are maintained.

1. If the complete set of subscript is
'indexes' (as a list), then it is always true that

   (append PREVIOUS-INDEXES (list INDEX) MORE-INDEXES) ==> indexes.

2. AXIS is he axis over which INDEX ranges (MATRIX indices are peculiar).


Arguments and Values:

INDEX : the current index; a fixnum, a boolean, a VECTOR, etc.
SHAPED-STRUCT : the ROBJ being accessed (something that can be the result of SHAPE.
AXIS  : the axis of the ARRAY where INDEX is situated
PREVIOUS-INDEXES : a list of the indexes already processed.
MORE-INDEXES     : a list of the indexes still to be processed.
result : a FUNCTION of one argument.


See also:

SHAPE


Notes:

The methods usually dispatch on the first two arguments.
"))


(defmethod at-fc-access ((idx fixnum) (a array) axis idxs more-idxs)
  (declare (type fixnum axis)
           (type cl:list idxs more-idxs))
  (if (null more-idxs)
      #'(lambda (arr)
          (declare (type array arr))
          (list (apply #'aref (array-data arr) (reverse (cons idx idxs)))))
      (at-fc-access (first more-idxs)
                    a
                    (1+ axis)
                    (cons idx idxs)
                    (rest more-idxs)
                    )))



(defmethod at-fc-access ((idx real) (a array) axis idxs more-idxs)
  (at-fc-access (truncate idx) a axis idxs more-idxs)
  )



(defun at-fc-access-symbol-string (idx a more-idxs axis idxs)
  (declare (type (or symbol string) idx)
           (type array a)
           (type fixnum axis)
           (type cl:list idxs more-idxs))
  (let* ((axis-names-pos (array-dimension-names-positions a axis))
         (idx-deref (and axis-names-pos
                         (gethash idx axis-names-pos)))
         )
    (declare (type (or null cl:hash-table) axis-names-pos)
             (type (or null cl:fixnum) idx-deref)
             (type cl:list element-indices)
             )
    (cond ((null idx-deref)
           (error "Unnown index name ~S on axis ~D."
                  idx
                  axis))
          
          ((null more-idxs)
           #'(lambda (arr)
               (declare (type array arr))
               (list (apply #'aref
                            (array-data arr)
                            ;; REVERSE is necessary instead of
                            ;; NREVERSE as IDXS may otherwise be
                            ;; munged by other "indexing" lambdas.
                            (reverse (cons idx-deref idxs))
                            ))))
        
          (t (at-fc-access (first more-idxs)
                           a
                           (1+ axis)
                           (cons idx-deref idxs)
                           (rest more-idxs)
                           ))))
  )



(defmethod at-fc-access ((idx cl:symbol) (a array) axis idxs more-idxs)
  (declare (type fixnum axis)
           (type cl:list idxs more-idxs))
  (let ((axis-names-pos (array-dimension-names-positions a axis)))
    (declare (type hash-table axis-names-pos))
    (if (null more-idxs)
        #'(lambda (arr)
            (declare (type array arr))
            (list (apply #'aref (array-data arr)
                         (reverse (cons (gethash idx axis-names-pos)
                                        idxs)))))
        (at-fc-access (first more-idxs)
                      a
                      (1+ axis)
                      (cons (gethash idx axis-names-pos) idxs)
                      (rest more-idxs)
                      )))
  )


(defmethod at-fc-access ((idx cl:string) (a array) axis idxs more-idxs)
  ;; Yes!  this is identical to the SYMBOL case.  Sorry!
  ;; But.... Let's try to be smart (or "tasteful") 20161124

  (declare (type fixnum axis)
           (type cl:list idxs more-idxs))
  (let* ((axis-names-pos (array-dimension-names-positions a axis))
         (idx-deref (and axis-names-pos
                         (gethash idx axis-names-pos)))
         )
    (declare (type (or null cl:hash-table) axis-names-pos)
             (type (or null cl:fixnum) idx-deref)
             (type cl:list element-indices)
             )
    (cond ((null idx-deref)
           (error "Unnown index name ~S on axis ~D."
                  idx
                  axis))
          
          ((null more-idxs)
           #'(lambda (arr)
               (declare (type array arr))
               (list (apply #'aref
                            (array-data arr)
                            ;; REVERSE is necessary instead of
                            ;; NREVERSE as IDXS may otherwise be
                            ;; munged by other "indexing" lambdas.
                            (reverse (cons idx-deref idxs))
                            ))))
        
          (t (at-fc-access (first more-idxs)
                           a
                           (1+ axis)
                           (cons idx-deref idxs)
                           (rest more-idxs)
                           ))))
  )


(defmethod at-fc-access ((idx (eql '*)) (a array) axis idxs more-idxs)
  (declare (type fixnum axis)
           (type cl:list idxs more-idxs))
  (if (null more-idxs)
      #'(lambda (arr)
          (declare (type array arr))
          (loop with adata = (array-data arr)
                for i from 0 below (array-dimension adata axis)
                collect
                (apply #'aref adata (reverse (cons i idxs)))
                ))

      #'(lambda (a)
          (declare (type array a))
          (mapcar #'(lambda (aref-f) (funcall aref-f a))
                  (loop for i from 0 below (array-dimension
                                            (array-data a)
                                            axis)
                        collect
                        (at-fc-access (first more-idxs)
                                      a
                                      (1+ axis)
                                      (cons i idxs)
                                      (rest more-idxs)))))))


(defmethod at-fc-access ((idx cl:vector) (a array) axis idxs more-idxs)
  (declare (type fixnum axis)
           (type cl:list idxs more-idxs))
  (if (null more-idxs)
      (cond ((is-fixnum-vector idx)
             (locally
               (declare (type (cl:vector fixnum) idx))
               #'(lambda (arr &aux (adata (array-data arr)))
                   (loop for vi across idx
                         collect
                         (apply #'aref adata (reverse (cons vi idxs))))))
             )
            ((is-real-vector idx)
             (let ((idx (map '(cl:vector fixnum) #'truncate idx)))
               (declare (type (cl:vector fixnum) idx))
               #'(lambda (arr &aux (adata (array-data arr)))
                   (loop for vi across idx
                         collect
                         (apply #'aref adata (reverse (cons vi idxs))))))
             )
            ((is-boolean-vector idx)
             ;; Need to fix 'recycling'.
             (locally
               (declare (type (cl:vector boolean) idx))
               #'(lambda (arr &aux (adata (array-data arr)))
                   (loop for vi across idx
                         for ti of-type fixnum from 0
                         when vi
                         collect
                         (apply #'aref adata (reverse (cons ti idxs))))))
             )
            ((is-names-vector idx)
             (let ((axis-dimnames-pos (array-dimension-names-positions a axis)))
               (declare (type (cl:vector (or string symbol)) idx)
                        (type hash-table axis-dimnames-pos))
               #'(lambda (arr &aux (adata (array-data arr)))
                   (loop for n across idx
                         collect
                         (apply #'aref adata
                                (reverse (cons (gethash n axis-dimnames-pos)
                                               idxs))))))
             )
            (t (error "INDEXING ERROR!"))
            )

      ;; Not done yet. MORE-IDXS to process.
      ;; Code structure mostly repeated from base case.
      (cond ((is-fixnum-vector idx)
             (locally
               (declare (type (cl:vector fixnum) idx))
               #'(lambda (a)
                   (declare (type array a))
                   (mapcar #'(lambda (aref-f) (funcall aref-f a))
                           (loop for i across idx
                                 collect
                                 (at-fc-access i
                                               a
                                               axis
                                               idxs
                                               more-idxs)))))
             )

            ((is-real-vector idx)
             (let ((idx (map '(cl:vector fixnum) #'truncate idx)))
               (declare (type (cl:vector fixnum) idx))
               #'(lambda (a)
                   (declare (type array a))
                   (mapcar #'(lambda (aref-f) (funcall aref-f a))
                           (loop for i across idx
                                 collect
                                 (at-fc-access i
                                               a
                                               axis
                                               idxs
                                               more-idxs)))))
             )
            ((is-boolean-vector idx)
             ;; Need to fix 'recycling'.
             (locally
               (declare (type (cl:vector boolean) idx))
               #'(lambda (a)
                   (declare (type array a))
                   (mapcar #'(lambda (aref-f) (funcall aref-f a))
                           (loop for i across idx
                                 for ti of-type fixnum from 0
                                 when i
                                 collect
                                 (at-fc-access ti
                                               a
                                               axis
                                               idxs
                                               more-idxs)))))
             )
            ((is-names-vector idx)
             (let ((axis-dimnames-pos (array-dimension-names-positions a axis)))
               (declare (type (cl:vector (or string symbol)) idx)
                        (type hash-table axis-dimnames-pos))
               #'(lambda (a)
                   (declare (type array a))
                   (mapcar #'(lambda (aref-f) (funcall aref-f a))
                           (loop for n across idx
                                 collect
                                 (at-fc-access (gethash n axis-dimnames-pos)
                                               a
                                               axis
                                               idxs
                                               more-idxs)))))
             )
            (t (error "INDEXING ERROR!"))
            )
      ))


(defmethod at-fc-access ((idx vector) (a array) axis idxs more-idxs)
  (declare (type fixnum axis)
           (type cl:list idxs more-idxs))
  (at-fc-access (vector-data idx) a axis idxs more-idxs))


(defmethod at-fc-access ((idx cl:list) (a array) axis idxs more-idxs)
  (declare (type fixnum axis)
           (type cl:list idxs more-idxs))
  (at-fc-access (coerce idx 'cl:vector) a axis idxs more-idxs))


;;; at-follow-dimnames
;;; ------------------
;;; 
;;; Notes:
;;; 20161226 MA:
;;; The :UNNAMED marker could be replaced by NULL.

(deftype result-dimensions ()
  `(or cl:sequence (eql :scalar) (eql :unnamed)))


(defgeneric at-follow-dimnames (index a dimnames result-dimnames axis more-idxs))


(defmethod at-follow-dimnames (
                               #+(or lispworks ccl sbcl cmucl)
                               (index fixnum)
                               #-(or lispworks ccl sbcl cmucl)
                               (index integer)
                               (a array)
                               dimnames
                               result-dimnames
                               axis
                               more-idxs)
  (declare (type cl:list dimnames result-dimnames more-idxs)
           (type cl:fixnum axis))
  (let* ((dn1 (first dimnames))
         (dname (if dn1 (list (elt dn1 index)) :scalar))
         )
    (declare (type result-dimensions dname))
    (if (null more-idxs)
        ;; (reverse (cons nil result-dimnames))
        (nreverse (cons dname result-dimnames)) 
        (at-follow-dimnames (first more-idxs)
                            a
                            (rest dimnames)
                            (cons dname result-dimnames)
                            (1+ axis)
                            (rest more-idxs))
        )))


(defmethod at-follow-dimnames ((index symbol) (a array)
                               dimnames
                               result-dimnames
                               axis
                               more-idxs)
  ;; Dispatch on multidimensional case.
  (at-follow-dimnames (list index) a dimnames result-dimnames axis more-idxs))


(defmethod at-follow-dimnames ((index string) (a array)
                               dimnames
                               result-dimnames
                               axis
                               more-idxs)
  ;; Exaclty the same behavior as the SYMBOL case.
  (at-follow-dimnames (list index) a dimnames result-dimnames axis more-idxs))


(defmethod at-follow-dimnames ((index (eql '*)) (a array)
                               dimnames
                               result-dimnames
                               axis
                               more-idxs)
  ;; Things get more interesting.
  (declare (type cl:list dimnames result-dimnames more-idxs)
           (type cl:fixnum axis))
  (let* ((dn1 (first dimnames))
         (dnames1 (or dn1 :unnamed))
         )
    (if (null more-idxs)
        (reverse (cons dnames1 result-dimnames))
        (at-follow-dimnames (first more-idxs)
                            a
                            (rest dimnames)
                            (cons dnames1 result-dimnames)
                            (1+ axis)
                            (rest more-idxs))
        )))


(defmethod at-follow-dimnames ((idx cl:vector) (a array)
                               dimnames
                               result-dimnames
                               axis
                               more-idxs)
  (let ((d1 (first dimnames)))
    (flet ((index-dim-names (d1 idx)
             (declare (type sequence d1)
                      (type (cl:vector fixnum) idx))
             (if d1
                 (loop for di of-type fixnum across idx
                       ;; The dimnames may be less than the actual
                       ;; range on the axis.
                       collect (ignore-errors (elt d1 di))
                       ;; Change to vectors!!!
                       )
                 :unnamed))

           (index-boolean-dim-names (d1 idx)
             (declare (type sequence d1)
                      (type (cl:vector boolean) idx))
             (if d1
                 (loop for di of-type boolean across idx
                       for d from 0 ; VECTORize!!!!
                       when di
                       collect (ignore-errors (elt d1 d))
                       )
                 :unnamed
                 ))
           )

      (if (null more-idxs)
          (cond ((is-fixnum-vector idx)
                 (locally
                   (declare (type (cl:vector fixnum) idx))
                   (reverse
                    (cons (index-dim-names d1 idx)
                          result-dimnames)))
                 )
                ((is-real-vector idx)
                 (let ((idx (map '(cl:vector fixnum) #'truncate idx)))
                   (declare (dynamic-extent idx)
                            (type (cl:vector fixnum) idx))
                   (reverse
                    (cons (index-dim-names d1 idx)
                          result-dimnames)))
                 )
                ((is-boolean-vector idx)
                 ;; Need to fix 'recycling'.
                 (locally
                   (declare (type (cl:vector boolean) idx))
                   (reverse
                    (cons (index-boolean-dim-names d1 idx)
                          result-dimnames)))
                 )
                ((is-names-vector idx)
                 ;; Assume everything is hanky-dory; besides, indexing
                 ;; would have raised an error before.

                 (reverse (cons (coerce idx 'list) result-dimnames))
                 )
                (t (error "DIMENSION NAMING ERROR!"))
                )

          ;; Not done yet. MORE-IDXS to process.
          ;; Code structure mostly repeated from base case.

          (cond ((is-fixnum-vector idx)
                 (locally
                   (declare (type (cl:vector fixnum) idx))
                   (at-follow-dimnames (first more-idxs)
                                       a
                                       (rest dimnames)
                                       (cons (index-dim-names d1 idx)
                                             result-dimnames)
                                       (1+ axis)
                                       (rest more-idxs)))
                 )
                ((is-real-vector idx)
                 (let ((idx (map '(cl:vector fixnum) #'truncate idx)))
                   (declare (type (cl:vector fixnum) idx))
                   (at-follow-dimnames (first more-idxs)
                                       a
                                       (rest dimnames)
                                       (cons (index-dim-names d1 idx)
                                             result-dimnames)
                                       (1+ axis)
                                       (rest more-idxs)))
                 )
                ((is-boolean-vector idx)
                 ;; Need to fix 'recycling'.
                 (locally
                   (declare (type (cl:vector boolean) idx))
                   (at-follow-dimnames (first more-idxs)
                                       a
                                       (rest dimnames)
                                       (cons (index-boolean-dim-names d1 idx)
                                             result-dimnames)
                                       (1+ axis)
                                       (rest more-idxs)))
                 )
                ((is-names-vector idx)
                 ;; Assume everything is hanky-dory; besides, indexing
                 ;; would have raised an error before.

                 (at-follow-dimnames (first more-idxs)
                                     a
                                     (rest dimnames)
                                     (cons (coerce idx 'list)
                                           result-dimnames)
                                     (1+ axis)
                                     (rest more-idxs))
                 )
                (t (error "DIMENSION NAMING ERROR!"))
                )
          ))))


(defmethod at-follow-dimnames ((idx cl:list) (a array)
                               dimnames
                               result-dimnames
                               axis
                               more-idxs)
  (at-follow-dimnames (coerce idx 'cl:vector)
                      a
                      dimnames
                      result-dimnames
                      axis
                      more-idxs))


(defmethod at-follow-dimnames ((idx vector) (a array)
                               dimnames
                               result-dimnames
                               axis
                               more-idxs)
  (at-follow-dimnames (vector-data idx)
                      a
                      dimnames
                      result-dimnames
                      axis
                      more-idxs))


(defmethod at-follow-dimnames ((index matrix) (a array)
                               dimnames
                               result-dimnames
                               axis
                               more-idxs)
  ;; Very special case.  FTTB we just drop the names.

  (declare (ignore dimnames
                   result-dimnames
                   axis
                   more-idxs
                   ))
  (make-list (nrow index))
  )


;;; AT
;;; ==

#| This needs reworking!
(defmethod at ((a array) idx &rest idxs)
  (if (and (typep idx 'fixnum)
           (every #'(lambda (i) (typep i 'fixnum)) idxs))
      (apply #'aref (array-data a) idx idxs)
      (let ((result (funcall (at-fc-access idx a 0 () idxs) a)))
        (if nil ; (= 1 (list-length result)) ; FTTB.
            (first result)
            (array result
                   :element-type (array-element-type a))))))
|#


(defmethod at ((a array) idx &rest idxs)
  (if idxs
      ;; More than one index.
      (let ((fixnum-idxs (ensure-fixnum-indices a (cons idx idxs))))
        (if fixnum-idxs
            (apply 'aref (array-data a) fixnum-idxs)
            (let ((result (funcall (at-fc-access idx a 0 () idxs) a)))
              (array result
                     :element-type (array-element-type a)))
            ))
      
      ;; Special, single index case.
      (let ((ns (names a))
            (adata (array-data a))
            (r (rank a))
            )
        (declare (ignore r)
                 (type cl:list ns)
                 (type cl:array adata))
        (multiple-value-bind (result el-name n/a)
            (typecase idx
              (fixnum
               (values (row-major-aref adata idx) ; Actually need to fix case of ARRAY with rank 1.
                       (ignore-errors (elt ns idx))))
              (real
               (values (row-major-aref adata (truncate idx))
                       (ignore-errors (elt ns idx))))
              ((or string symbol)
               (let* ((npos (array-names-pos a))
                      (n-idx (and npos (gethash idx npos)))
                      )
                 (if (and ns n-idx)
                     (values (row-major-aref adata n-idx) idx)
                     (values nil nil t))))
              )
          (declare (ignore n/a))
          (cond ((and ns (not *drop*) (not n/a))
                 (vector* (list result)
                          :names (list el-name)
                          :element-type (array-element-type a)))
                ((and ns (not *drop*) n/a)
                 (vector* (list result)
                          :names (list nil)
                          :element-type (array-element-type a)))
                (t ; (or (null ns) *drop*)
                 result))
          ))
      ))


(defmethod at ((a array) (r null) &rest c-etc)
  (declare (ignore c-etc))
  ;; (assert (null c-etc))

  ;; Simple FTTB.
  (aref (array-data a)))


(defmethod at ((a array) (index vector) &rest idxs)
  (apply #'at a (vector-data index) idxs))


(defmethod at ((a array) (index cl:list) &rest idxs)
  (apply #'at a (coerce index 'cl:vector) idxs))


(defmethod at ((a array) (index cl:vector) &rest idxs)
  (if idxs
      ;; Full call.
      (array (funcall (at-fc-access index a 0 () idxs) a)
             :element-type (array-element-type a))
      
      ;; Access elements indexed in VECTOR V in ROW-MAJOR order.
      (let* ((ad (array-data a))
             (i index)
             (v (make-array (array-total-size ad)
                            :displaced-to ad
                            :element-type (cl:array-element-type ad)
                            ))
             )
        (declare (type cl:array ad)
                 (type cl:vector i v)
                 (dynamic-extent v))

        (cond ((is-fixnum-vector i)
               (extract-fixunm-indexed-elements a v index i))

              ((is-real-vector index)
               (let ((i (map '(vector fixnum) #'truncate i)))
                 (extract-fixunm-indexed-elements a v index i)))

              ((is-boolean-vector i)
               (extract-boolean-indexed-elements a v index i))

              ((is-names-vector i)
               (extract-name-indexed-elements a v index i))

              (t (warn "Not yet implemented.") nil)
              )
        ))
  )


;;; This is the working version.  Still gets called too many times
;;; because of the dispatching between list, string and vectors, but
;;; it works.

#| Old version

(defmethod at :around ((a array) index &rest idxs)
  (let* ((result (call-next-method))
         (dim-names ;(if idxs
                        (at-follow-dimnames index
                                            a
                                            (dimension-names a)
                                            ()
                                            0
                                            idxs)
                        ;(names a)
                        ;)
                        )

         ;; Remove the leading NILs of single dimension from DIM-NAMES
         ;; and USE that for resulting dimension-names.

         ;; (result-dims (remove nil dim-names))
         #|
         (result-dims (loop for dims on dim-names
                            ;; Should collapse the test to (<= (length (first dims))).
                            while (or (null (first dims))
                                      (= 1 (length (first dims)))
                                      )
                            finally (return dims))
                      )
         |#
         (result-dims
          (substitute nil :scalar
                      (substitute nil :unnamed
                                  (loop for dims on dim-names
                                        while (eq :scalar (first dims))
                                        finally (return dims))
                                  :test #'eq)
                      :test #'eq)
          )

         (result-rank
          ;; (count-if-not #'is-scalar-index (cons index idxs))
          (list-length result-dims))
         )
    (declare (type cl:list dim-names result-dims)
             (type fixnum result-rank))

    (when (plusp result-rank)
      (cond ((= 1 result-rank (cl:list-length result-dims))
             ;; Result is essentially a vector.
             (setf (names result) (first result-dims)
                   (dimension-names result) result-dims)
             )

            ((= result-rank (cl:list-length result-dims))
             ;; Some initial dimensions may have been dropped.
             (setf (dimension-names result) result-dims))

            ((and (> result-rank (cl:list-length result-dims))
                  (= result-rank (cl:list-length dim-names)))
             ;; Initial dimensions must not have been dropped.
             (setf (dimension-names result) dim-names))
            ))
    result))
|#

;;; New version running once, only when the global *at-around-run* is
;;; 0. Kludgy but effective.

;;; *at-around-running*
;;; -------------------
;;; Kludgy flag to make AT :AROUND methods run only once despite the
;;; dispatching. May mess up multithreading, but, we'll look into that
;;; later.

(defvar *at-around-running* -1)

(declaim (type fixnum *at-around-running*))


;;; at :around
;;; ----------
;;; This is where we fix the naming of dimensions etc.  Must rely on
;;; *AT-AROUND-RUNNING*, which must be -1 (the invariant) once all the
;;; :around calls are done. Works for nested calls because of order of
;;; evaluation.
;;;
;;; Notes:
;;;
;;; 20161226 MA:
;;; Will not work for delayed forms of evaluation.

(defmethod at :around ((a array) index &rest idxs)
  (unwind-protect
      (progn
        (incf *at-around-running*) ; First time around becomes 0.

        (let ((result (call-next-method)))

          (unless (plusp *at-around-running*) 
            ;; Do not run the names and dimnames settings when
            ;; dispatching.

            (let* ((dim-names
                    (at-follow-dimnames index
                                        a
                                        (dimension-names a)
                                        ()
                                        0
                                        idxs)
                    )

                   ;; Remove the leading :SCALAR single dimension from DIM-NAMES
                   ;; and USE that for resulting dimension-names, after
                   ;; having substituted back NIL for the :SCALAR or
                   ;; :UNNAMED dimensions.

                   (res-dims
                    (loop for dims on dim-names
                          while (eq :scalar (first dims))
                          finally (return dims))
                    )

                   (result-rank
                    ;; (list-length result-dims)
                    ;; (1+ (count-if-not #'(lambda (d) (eq :scalar d)) res-dims))
                    (count-if-not #'is-scalar-index (cons index idxs))
                    )

                   (result-dims
                    (substitute nil :scalar
                                (substitute nil :unnamed
                                            res-dims
                                            :test #'eq)
                                :test #'eq)
                    )
                   )
              (declare (type cl:list dim-names result-dims)
                       (type fixnum result-rank))

              (when (plusp result-rank)
                (cond ((= 1 result-rank (cl:list-length result-dims))
                       ;; Result is essentially a vector.
                       (setf (names result) (first result-dims)
                             (dimension-names result) result-dims)
                       )

                      ((= result-rank (cl:list-length result-dims))
                       ;; Some initial dimensions may have been dropped.
                       (setf (dimension-names result) result-dims))

                      ((and (> result-rank (cl:list-length result-dims))
                            (= result-rank (cl:list-length dim-names)))
                       ;; Initial dimensions must not have been dropped.
                       (setf (dimension-names result) dim-names))
                      )))
            )
          result))

    ;; Cleanup form.
    (decf *at-around-running*))
  )


;;; (SETF AT)
;;; =========

;;; setf-at-fc-access
;;; -----------------
;;; Note that the methods dispatch on the first three arguments.  The
;;; code for the non atomic values slices is organized by first giving
;;; the code for the methods having as second argument a ROBJ, e.g.,
;;; ARRAY and then varying the other two.  Methods on CL objects only,
;;; if they exist, are listed later.

(defgeneric setf-at-fc-access (index
                               shaped-struct
                               value
                               axis 
                               previous-indexes
                               more-indexes
                               &optional
                               value-indexes
                               )
  (:documentation "Creates a function that sets elements in a SHAPED-STRUCT.

The methods of this generic function build up a lambda in an almost
continuation passing style; during the execution the following
invariants are maintained.

1. If the complete set of subscript is
'indexes' (as a list), then it is always true that

   (append PREVIOUS-INDEXES (list INDEX) MORE-INDEXES) ==> indexes.

2. AXIS is he axis over which INDEX ranges (MATRIX indices are peculiar).


Arguments and Values:

INDEX : the current index; a fixnum, a boolean, a VECTOR, etc.
SHAPED-STRUCT : the ROBJ being accessed (something that can be the result of SHAPE.
VALUE : the value to be set at 'indexes'.
AXIS  : the axis of the ARRAY where INDEX is situated
PREVIOUS-INDEXES : a list of the indexes already processed.
MORE-INDEXES     : a list of the indexes still to be processed.
VALUE-INDEXES    : a list of indices in the VALUE object (if necessary).
result : a FUNCTION of one argument.


See also:

SHAPE


Notes:

The methods usually dispatch on the first three arguments.
"))

#|
The specializations on first and third arguments; second arguments in
this file is always RHO:ARRAY

Kind of "setting"	INDEX		SHAPED-STRUCT	Missing?
			Arg 1 Class	Arg 2 Class
----------------------------------------------------------------
Atomic
			FIXNUM 		T
                        FIXNUM		CL:VECTOR
                        FIXNUM		VECTOR
			FIXNUM		CL:ARRAY	T
			FIXNUM		ARRAY

                        SYMBOL		T
                        SYMBOL		CL:VECTOR	T
                        SYMBOL		VECTOR		T

                        STRING		T
                        STRING		CL:VECTOR	T
                        STRING		VECTOR		T

Slice
			(EQL '*)	T
			(EQL '*)	CL:VECTOR
			(EQL '*)	VECTOR
			(EQL '*)	CL:ARRAY
			(EQL '*)	ARRAY

			CL:VECTOR	T
			CL:VECTOR	CL:VECTOR
			CL:VECTOR	VECTOR		T
			CL:VECTOR	CL:ARRAY
			CL:VECTOR	ARRAY

			VECTOR		T		T
			VECTOR		CL:VECTOR	T
			VECTOR		VECTOR		T
			VECTOR		CL:ARRAY	T
			VECTOR		ARRAY

|#

;;; Auxiliary Functions for (setf at)
;;; ---------------------------------

(defun setf-array-elements-1 (adata indexes value)
  (declare (type cl:list indexes)
           (type cl:array adata))

  #| Originating code:

  (setf (apply #'aref adata (reverse (cons i idxs)))
        value)
  |#

  (setf (apply #'aref adata (reverse indexes)) value)
  )


(defun setf-array-elements (adata indexes value value-indexes)
  (declare (type cl:list indexes value-indexes)
           (type cl:array adata value))

  #| Originating code:
  (setf-array-elements (i j)
           (declare (type fixnum i j))
           (setf (apply #'aref adata (reverse (cons i idxs)))
                 (apply #'aref value (reverse (cons j value-indexes)))))

  |#

  (setf (apply #'aref adata (reverse indexes))
        (apply #'aref value (reverse value-indexes)))
  )

(declaim (inline setf-array-elements-1 setf-array-elements))


;;; Atomic Methods
;;; --------------

(defmethod setf-at-fc-access (
                              ;; FIXNUM is surely a TYPE, but not always a CLASS.
                              #+(or lispworks ccl sbcl cmucl)
                              (idx fixnum)
                              #-(or lispworks ccl sbcl cmucl)
                              (idx integer)
                              (a array)
                              value
                              axis
                              idxs
                              more-idxs
                              &optional
                              value-indexes
                              &aux
                              (adata (array-data a))
                              )
  (declare (ignore value-indexes)
           (type fixnum axis)
           (type cl:list idxs more-idxs))
  (if (null more-idxs)
      #'(lambda (arr)
          (declare (type array arr) (ignore arr))
          (setf (apply #'aref adata
                       (reverse (cons idx idxs)))
                value))
      (setf-at-fc-access (first more-idxs)
                         a
                         value
                         (1+ axis)
                         (cons idx idxs)
                         (rest more-idxs)
                         ))
  )


(defmethod setf-at-fc-access (
                              ;; FIXNUM is surely a TYPE, but not always a CLASS.
                              #+(or lispworks ccl sbcl cmucl)
                              (idx fixnum)
                              #-(or lispworks ccl sbcl cmucl)
                              (idx integer)
                              (a array)
                              (value cl:vector)
                              axis
                              idxs
                              more-idxs
                              &optional
                              value-indexes
                              &aux
                              (adata (array-data a))
                              )
  (declare (type fixnum axis)
           (type cl:list idxs more-idxs))
  (if (null more-idxs)
      #'(lambda (arr)
          (declare (type array arr) (ignore arr))
          (setf (apply #'aref adata (reverse (cons idx idxs)))
                (apply #'aref value (reverse value-indexes)))
          value)
      (setf-at-fc-access (first more-idxs)
                         a
                         value
                         (1+ axis)
                         (cons idx idxs)
                         (rest more-idxs)
                         value-indexes
                         ))
  )


(defmethod setf-at-fc-access ((idx cl:symbol)
                              (a array)
                              value
                              axis
                              idxs
                              more-idxs
                              &optional
                              value-indexes
                              &aux
                              (adata (array-data a))
                              )
  (declare (ignore value-indexes)
           (type fixnum axis)
           (type cl:list idxs more-idxs))
  (let* ((axis-names-pos (array-dimension-names-positions a axis))
         (idx-deref (and axis-names-pos
                         (gethash idx axis-names-pos)))
         )
    (declare (type (or null cl:hash-table) axis-names-pos)
             (type (or null cl:fixnum) idx-deref)
             (type cl:list element-indices)
             )
    (cond ((null idx-deref)
           (error "Unnown index name ~S on axis ~D."
                  idx
                  axis))
          
          ((null more-idxs)
           #'(lambda (arr)
               (declare (type array arr) (ignore arr))
               (setf (apply #'aref adata
                            (reverse (cons idx-deref idxs))
                            )
                     value)))
        
          (t (setf-at-fc-access (first more-idxs)
                                a
                                value
                                (1+ axis)
                                (cons idx-deref idxs)
                                (rest more-idxs)
                                ))))
  )


(defmethod setf-at-fc-access ((idx cl:string)
                              (a array)
                              value
                              axis
                              idxs
                              more-idxs
                              &optional
                              value-indexes
                              &aux
                              (adata (array-data a))
                              )
  ;; Just factor out the code for SYMBOL and STRING!!!!
  (declare (ignore value-indexes)
           (type fixnum axis)
           (type cl:list idxs more-idxs))
  (let* ((axis-names-pos (array-dimension-names-positions a axis))
         (idx-deref (and axis-names-pos
                         (gethash idx axis-names-pos)))
         )
    (declare (type (or null cl:hash-table) axis-names-pos)
             (type (or null cl:fixnum) idx-deref)
             (type cl:list element-indices)
             )
    (cond ((null idx-deref)
           (error "Unnown index name ~S on axis ~D."
                  idx
                  axis))
          
          ((null more-idxs)
           #'(lambda (arr)
               (declare (type array arr) (ignore arr))
               (setf (apply #'aref adata
                            (reverse (cons idx-deref idxs))
                            )
                     value)))
        
          (t (setf-at-fc-access (first more-idxs)
                                a
                                value
                                (1+ axis)
                                (cons idx-deref idxs)
                                (rest more-idxs)
                                ))))
  )


;;; Slice methods
;;; -------------

;;; Slice set in pointwise fashion to an "atomic" value.

(defmethod setf-at-fc-access ((idx (eql '*))
                              (a array)
                              (value t) ; Generic case.
                              axis
                              idxs
                              more-idxs
                              &optional
                              value-indexes
                              &aux
                              (adata (array-data a))
                              )
  (declare (ignore value-indexes)
           (type fixnum axis)
           (type cl:array adata)
           (type cl:list idxs more-idxs))
  (if (null more-idxs)
      #'(lambda (arr)
          (declare (type array arr) (ignore arr))
          (loop for i from 0 below (array-dimension adata axis)
                
                do (setf (apply #'aref adata (reverse (cons i idxs)))
                         value)
                finally (return value)
                ))

      #'(lambda (a)
          (declare (type array a) (ignore arr))
          (loop with (fmi . rmi) = more-idxs
                with ax1 of-type fixnum = (1+ axis)
                for i from 0 below (array-dimension adata axis)
                do (funcall
                    (setf-at-fc-access fmi ; (first more-idxs)
                                       a
                                       value
                                       ax1 ; (1+ axis)
                                       (cons i idxs)
                                       rmi ; (rest more-idxs)
                                       )
                    a)
                finally (return value)))
      ))


;;; Slice set to "non atomic" value.

(defmethod setf-at-fc-access ((idx (eql '*))
                              (a array)
                              (value cl:vector) ; CL:VECTOR
                              axis
                              idxs
                              more-idxs
                              &optional
                              value-indexes
                              &aux
                              (adata (array-data a))
                              )
  (declare (ignore value-indexes)
           (type fixnum axis)
           (type cl:array adata)
           (type cl:list idxs more-idxs))
  (if (null more-idxs)
      #'(lambda (arr)
          (declare (type array arr) (ignore arr))
          (loop for i from 0 below (array-dimension adata axis)
                
                do (setf (apply #'aref adata (reverse (cons i idxs)))
                         (aref value i))
                finally (return value)
                ))

      #'(lambda (a)
          (declare (type array a) (ignore arr))
          (loop with (fmi . rmi) = more-idxs
                with ax1 = (1+ axis)
                for i from 0 below (array-dimension adata axis)
                do (funcall
                    (setf-at-fc-access fmi ; (first more-idxs)
                                       a
                                       (aref value i)
                                       ax1 ; (1+ axis)
                                       (cons i idxs)
                                       rmi ; (rest more-idxs)
                                       )
                    a)
                finally (return value)))
      ))


(defmethod setf-at-fc-access ((idx (eql '*))
                              (a array)
                              (value vector)
                              axis
                              idxs
                              more-idxs
                              &optional
                              value-indexes
                              )
  (declare (ignore value-indexes)
           (type fixnum axis)
           (type cl:list idxs more-idxs))
  #'(lambda (a)
      (declare (type array a))
      (funcall (setf-at-fc-access '* a (vector-data value) axis idxs more-idxs) a)
      value))
  

(defmethod setf-at-fc-access ((idx (eql '*))
                              (a array)
                              (value cl:array) ; CL:ARRAY
                              axis
                              idxs
                              more-idxs
                              &optional
                              value-indexes
                              &aux
                              (adata (array-data a))
                              )
  (declare (type fixnum axis)
           (type cl:array adata)
           (type cl:list idxs more-idxs))
  (if (null more-idxs)
      #'(lambda (arr)
          (declare (type array arr) (ignore arr))
          (loop for i from 0 below (array-dimension adata axis)
                for j from 0 below (array-dimension value (1- (array-rank value)))
                
                do (setf (apply #'aref adata (reverse (cons i idxs)))
                         (apply #'aref value (reverse (cons j value-indexes))))
                finally (return value)
                ))

      #'(lambda (a)
          (declare (type array a) (ignore arr))
          (loop with (fmi . rmi) = more-idxs
                with ax1 = (1+ axis)
                for i of-type fixnum below (array-dimension adata axis)
                do (funcall
                    (setf-at-fc-access fmi ; (first more-idxs)
                                       a
                                       value
                                       ax1 ; (1+ axis)
                                       (cons i idxs)
                                       rmi ; (rest more-idxs)
                                       (cons i value-indexes)
                                       )
                    a)
                finally (return value)))
      ))


(defmethod setf-at-fc-access ((idx (eql '*))
                              (a array)
                              (value array)
                              axis
                              idxs
                              more-idxs
                              &optional
                              value-indexes
                              )
  (declare (type fixnum axis)
           (type cl:list idxs more-idxs))
  #'(lambda (a)
      (declare (type array a))
      (funcall (setf-at-fc-access '*
                                  a
                                  (array-data value)
                                  axis
                                  idxs
                                  more-idxs
                                  value-indexes)
               a)
      value))


(defmethod setf-at-fc-access ((idx cl:vector)
                              (a array)
                              (value T) ; Value is "atomic".
                              axis
                              idxs
                              more-idxs
                              &optional
                              value-indexes
                              &aux
                              (adata (array-data a))
                              )
  (declare (type fixnum axis)
           (type cl:array adata)
           (type cl:list idxs more-idxs))
  (flet ((setf-array-elements-1 (i)
           (declare (type fixnum i))
           (setf (apply #'aref adata (reverse (cons i idxs)))
                 value))
         )
    (if (null more-idxs)
        (cond ((is-fixnum-vector idx)
               (locally
                 (declare (type (cl:vector fixnum) idx))
                 #'(lambda (arr)
                     (declare (type array arr) (ignore arr))
                     (loop for i of-type fixnum across idx
                           do (setf-array-elements-1 i)
                           finally (return value)
                           ))
                 ))

              ((is-real-vector idx)
               (locally
                 (declare (type (cl:vector real) idx))
                 #'(lambda (arr)
                     (declare (type array arr) (ignore arr))
                     (loop for i of-type real across idx
                           do (setf-array-elements-1 (truncate i))
                           finally (return value)))
                 ))

              ((is-boolean-vector idx)
               ;; Need to fix 'recycling'.
               (locally
                 (declare (type (cl:vector boolean) idx))
                 #'(lambda (arr)
                     (declare (type array arr) (ignore arr))
                     (loop for i  of-type boolean across idx
                           for vi of-type fixnum from 0
                           when i
                           do (setf-array-elements-1 vi)
                           finally (return value)))
                 ))

              ((is-names-vector idx)
               (let ((axis-dimnames-pos (array-dimension-names-positions a axis)))
                 (declare (type (cl:vector (or string symbol)) idx)
                          (type hash-table axis-dimnames-pos))
                 #'(lambda (arr)
                     (declare (type array arr) (ignore arr))
                     (loop for n across idx
                           do (setf-array-elements-1 (gethash n axis-dimnames-pos))
                           finally (return value)))
                 ))

              (t (error "SETF INDEXING ERROR!"))
              )

        ;; There are MORE-IDXS.
        (cond ((is-fixnum-vector idx)
               (locally
                 (declare (type (cl:vector fixnum) idx))
                 #'(lambda (a)
                     (declare (type array a) (ignore arr))
                     (loop with (fmi . rmi)
                           with ax1 = (1+ axis)
                           for i of-type fixnum across idx
                           do (funcall
                               (setf-at-fc-access fmi ; (first more-idxs)
                                                  a
                                                  value
                                                  ax1 ; (1+ axis)
                                                  (cons i idxs)
                                                  rmi ; (rest more-idxs)
                                                  (cons i value-indexes)
                                                  )
                               a)
                           finally (return value)))
                 ))

              ((is-real-vector idx)
               (locally
                 (declare (type (cl:vector real) idx))
                 #'(lambda (a)
                     (declare (type array a))
                     (loop with (fmi . rmi) = more-idxs
                           with ax1 of-type fixnum = (1+ axis)
                           for i of-type real across idx
                           for ti of-type fixnum = (truncate i)
                           do (funcall
                               (setf-at-fc-access fmi ; (first more-idxs)
                                                  a
                                                  value
                                                  ax1 ; (1+ axis)
                                                  (cons ti idxs)
                                                  rmi ; (rest more-idxs)
                                                  (cons ti value-indexes)
                                                  )
                               a)
                           finally (return value)))
                 ))

              ((is-boolean-vector idx)
               (locally
                 (declare (type (cl:vector boolean) idx))
                 #'(lambda (a)
                     (declare (type array a))
                     (loop with j of-type fixnum = -1
                           with (fmi . rmi) = more-idxs
                           with ax1 = (1+ axis)
                           for i across idx
                           for ti of-type fixnum from 0
                           when i
                           do (incf j)
                           and do (funcall
                                   (setf-at-fc-access fmi ; (first more-idxs)
                                                      a
                                                      value
                                                      ax1 ; (1+ axis)
                                                      (cons ti idxs)
                                                      rmi ; (rest more-idxs)
                                                      (cons j value-indexes)
                                                      )
                                   a)
                           finally (return value)))
                 ))

              (t (error "SETF INDEXING ERROR (not implemented?)"))
              ))
    ))


(defmethod setf-at-fc-access ((idx cl:vector)
                              (a array)
                              (value cl:array) ; CL:ARRAY
                              axis
                              idxs
                              more-idxs
                              &optional
                              value-indexes
                              &aux (adata (array-data a))
                              )
  (declare (type fixnum axis)
           (type cl:array adata)
           (type cl:list idxs more-idxs))
  (flet ((setf-array-elements (i j)
           (declare (type fixnum i j))
           (setf (apply #'aref adata (reverse (cons i idxs)))
                 (apply #'aref value (reverse (cons j value-indexes)))))
         )
    (if (null more-idxs)
        (cond ((is-fixnum-vector idx)
               (locally
                 (declare (type (cl:vector fixnum) idx))
                 #'(lambda (arr)
                     (declare (type array arr) (ignore arr))
                     (loop for i across idx
                           for j from 0 ; Dangerous!

                           do (setf-array-elements i j)
                           finally (return value)
                           ))
                 ))

              ((is-real-vector idx)
               (locally
                 (declare (type (cl:vector real) idx))
                 #'(lambda (arr)
                     (declare (type array arr) (ignore arr))
                     (loop for i of-type real across idx
                           for j from 0 ; Dangerous!
                         
                           do (setf-array-elements (truncate i) j)
                           finally (return value)))
                 ))

              ((is-boolean-vector idx)
               ;; Need to fix 'recycling'.
               (locally
                 (declare (type (cl:vector boolean) idx))
                 #'(lambda (arr)
                     (declare (type array arr) (ignore arr))
                     (loop for i  of-type boolean across idx
                           for vi of-type fixnum from 0
                           for j  of-type fixnum from 0 ; Dangerous!
                         
                           when i
                           do (setf-array-elements vi j)
                           finally (return value)))
                 ))

              ((is-names-vector idx)
               (let ((axis-dimnames-pos (array-dimension-names-positions a axis)))
                 (declare (type (cl:vector (or string symbol)) idx)
                          (type hash-table axis-dimnames-pos))
                 #'(lambda (arr)
                     (declare (type array arr) (ignore arr))
                     (loop for n across idx
                           for j of-type fixnum from 0 ; Dangerous!
                           do (setf-array-elements (gethash n axis-dimnames-pos) j)
                           finally (return value)))
                 ))

              (t (error "SETF INDEXING ERROR!"))
              )

        ;; There are MORE-IDXS.
        (let ((fmi (first more-idxs))
              (rmi (rest more-idxs))
              (ax1 (1+ axis))
              )
          (declare (type fixnum ax1))
          (cond ((is-fixnum-vector idx)
                 (locally
                   (declare (type (cl:vector fixnum) idx))
                   #'(lambda (a)
                       (declare (type array a))
                       (loop for i across idx
                             for j from 0
                             do (funcall
                                 (setf-at-fc-access fmi ; (first more-idxs)
                                                    a
                                                    value
                                                    ax1 ; (1+ axis)
                                                    (cons i idxs)
                                                    rmi ; (rest more-idxs)
                                                    (cons j value-indexes)
                                                    )
                                 a)
                             finally (return value)))
                   ))

                ((is-real-vector idx)
                 (locally
                   (declare (type (cl:vector real) idx))
                   #'(lambda (a)
                       (declare (type array a))
                       (loop for i of-type real across idx
                             for ti = (truncate i)
                             do
                             (funcall
                              (setf-at-fc-access fmi ; (first more-idxs)
                                                 a
                                                 value
                                                 ax1 ; (1+ axis)
                                                 (cons ti idxs)
                                                 rmi ; (rest more-idxs)
                                                 (cons ti value-indexes)
                                                 )
                              a)
                             finally (return value)))
                   ))


                ((is-boolean-vector idx)
                 (locally
                   (declare (type (cl:vector boolean) idx))
                   #'(lambda (a)
                       (declare (type array a))
                       (loop with j of-type fixnum = -1
                             for i across idx
                             for ti of-type fixnum from 0
                             when i
                             do (incf j)
                             and do
                             (funcall
                              (setf-at-fc-access fmi ; (first more-idxs)
                                                 a
                                                 value
                                                 ax1 ; (1+ axis)
                                                 (cons ti idxs)
                                                 rmi ; (rest more-idxs)
                                                 (cons j value-indexes)
                                                 )
                              a)
                             finally (return value)))
                   ))

                (t (error "SETF INDEXING ERROR (not implemented?)"))
                )
          ))
    ))


(defmethod setf-at-fc-access ((idx cl:vector)
                              (a array)
                              (value array) ; ARRAY
                              axis
                              idxs
                              more-idxs
                              &optional
                              value-indexes
                              )
  (setf-at-fc-access idx
                     a
                     (array-data value)
                     axis
                     idxs
                     more-idxs
                     value-indexes))


(defmethod setf-at-fc-access ((idx vector)
                              (a array)
                              (value cl:array) ; CL:ARRAY
                              axis
                              idxs
                              more-idxs
                              &optional
                              value-indexes
                              )
  (setf-at-fc-access (vector-data idx)
                     a
                     value
                     axis
                     idxs
                     more-idxs
                     value-indexes))


(defmethod setf-at-fc-access ((idx vector)
                              (a array)
                              (value array) ; ARRAY
                              axis
                              idxs
                              more-idxs
                              &optional
                              value-indexes
                              )
  (setf-at-fc-access (vector-data idx)
                     a
                     (array-data value)
                     axis
                     idxs
                     more-idxs
                     value-indexes))


;;; setf-at-fc-access cl:vector array cl:vector
;;; -------------------------------------------
;;; This is tricky as the index into the value must be incremented
;;; between calls. To achieve this result we pass the 'last index' in
;;; the VALUE in VALUE-INDEXES.

#|
(defmethod setf-at-fc-access ((idx cl:vector)
                              (a array)
                              (value cl:vector) ; CL:VECTOR
                              axis
                              idxs
                              more-idxs
                              &optional
                              (value-indexes '(0 0))
                              &aux
                              (idx-length (cl:length idx))
                              (adata (array-data a))
                              (value-index (first value-indexes))
                              (value-offset (second value-indexes))
                              )
  (declare (type fixnum axis value-index)
           (type cl:list idxs more-idxs value-indexes)
           (type cl:array adata))
  (flet ((setf-array-elements (i j)
           (declare (type fixnum i j))
           (format t ">>> setting at ~S ~S.~%"
                   (reverse (cons i idxs))
                   j)
           (setf (apply #'aref adata (reverse (cons i idxs)))
                 (aref value j)))
         )
    (if (null more-idxs)
        (cond ((is-fixnum-vector idx)
               (locally
                 (declare (type (cl:vector fixnum) idx))
                 #'(lambda (arr)
                     (declare (type array arr) (ignore arr))
                     (loop for i across idx
                           for offset from 0
                           ;; for j from 0 ; Dangerous
                           for j of-type fixnum
                           = value-index
                           then (+ value-index (* offset idx-length))
                           do (format t ">>> about to set ~S ~S (o: ~S vo: ~S).~%"
                                      i j offset value-offset)
                           do (setf-array-elements i j)
                           finally (return value)
                           ))
                 ))

              ((is-real-vector idx)
               (locally
                 (declare (type (cl:vector real) idx))
                 #'(lambda (arr)
                     (declare (type array arr) (ignore arr))
                     (loop for i of-type real across idx
                           ;; for j of-type fixnum from 0 ; Dangerous
                           for j of-type fixnum from value-index
                           do (setf-array-elements (truncate i) j)
                           finally (return value)))
                 ))

              ((is-boolean-vector idx)
               ;; Need to fix 'recycling'.
               (locally
                 (declare (type (cl:vector boolean) idx))
                 #'(lambda (arr)
                     (declare (type array arr) (ignore arr))
                     (loop for i  of-type boolean across idx
                           for vi of-type fixnum from 0
                           ;; for j  of-type fixnum from 0 ; Dangerous!
                           for j of-type fixnum from value-index
                           when i
                           do (setf-array-elements vi j)
                           finally (return value)))
                 ))

              ((is-names-vector idx)
               (let ((axis-dimnames-pos (array-dimension-names-positions a axis)))
                 (declare (type (cl:vector (or string symbol)) idx)
                          (type hash-table axis-dimnames-pos))
                 #'(lambda (arr)
                     (declare (type array arr) (ignore arr))
                     (loop for n across idx
                           ;; for j of-type fixnum from 0 ; Dangerous!
                           for j of-type fixnum from value-index
                           do (setf-array-elements (gethash n axis-dimnames-pos) j)
                           finally (return value)))
                 ))

              (t (error "SETF INDEXING ERROR!"))
              )

        ;; There are MORE-IDXS.
        (let ((fmi (first more-idxs))
              (rmi (rest more-idxs))
              (ax1 (1+ axis))
              )
          (declare (type fixnum ax1))
          (cond ((is-fixnum-vector idx)
                 (locally
                   (declare (type (cl:vector fixnum) idx))
                   #'(lambda (a)
                       (declare (type array a))
                       (loop for i of-type fixnum across idx
                             ;; for j from 0 ; Dangerous
                             for j of-type fixnum
                             from value-index ; by idx-length
                             for offset from 0
                             do (format t ">> Iteration axis: ~S ids: ~S j: ~S offset ~S.~%"
                                        axis
                                        (cons i idxs)
                                        j
                                        offset)
                             do (funcall
                                 (setf-at-fc-access fmi ; (first more-idxs)
                                                    a
                                                    value
                                                    ax1 ; (1+ axis)
                                                    (cons i idxs)
                                                    rmi ; (rest more-idxs)
                                                    (list j offset)
                                                    ;; (cons j value-indexes)
                                                    ;; The above should always be just (LIST J)
                                                    )
                                 a)
                             finally (return value)))
                   ))

                ((is-real-vector idx)
                 (locally
                   (declare (type (cl:vector real) idx))
                   #'(lambda (a)
                       (declare (type array a))
                       (loop for i of-type real across idx
                             ;; for j from 0
                             for j  of-type fixnum
                             from value-index ; by idx-length
                             do (funcall
                                 (setf-at-fc-access fmi ; (first more-idxs)
                                                    a
                                                    value
                                                    ax1 ; (1+ axis)
                                                    (cons (truncate i) idxs)
                                                    rmi ; (rest more-idxs)
                                                    (list j)
                                                    ;; j
                                                    )
                                 a)
                             finally (return value)))
                   ))

                ((is-boolean-vector idx)
                 (locally
                   (declare (type (cl:vector boolean) idx))
                   #'(lambda (a)
                       (declare (type array a))
                       (loop with j of-type fixnum = value-index
                             for i of-type boolean across idx
                             for ti of-type fixnum from 0
                             ;; for vj from 0 ; Check this, wrt J.
                             when i
                             do (incf j #|idx-length|#)
                             and do (funcall
                                     (setf-at-fc-access fmi ; (first more-idxs)
                                                        a
                                                        value
                                                        ax1 ; (1+ axis)
                                                        (cons ti idxs)
                                                        rmi ; (rest more-idxs)
                                                        ;; (list vj)
                                                        (list j)
                                                        ;; j
                                                        )
                                     a)
                             finally (return value)))
                   ))

                (t (error "SETF INDEXING ERROR (not implemented?)"))
                )
          ))
    ))
|#

;;; New attempt.  Let's pass indices as if for an array and let's
;;; compute the vector index.
;;;
;;; In value-indexes we pass pairs
;;; ( <dimension index>  <result dimension size>)
;;; See DEFSTRUCT below.

(defstruct (vector-index-info (:type list)
                              (:constructor vi-info (vi-index vi-dim))
                              (:conc-name nil))
  (vi-index 0 :type fixnum :read-only t)
  (vi-dim 0 :type fixnum :read-only t))

  
(defmethod setf-at-fc-access ((idx cl:vector)
                              (a array)
                              (value cl:vector) ; CL:VECTOR
                              axis
                              idxs
                              more-idxs
                              &optional
                              value-indexes
                              &aux
                              (idx-length (cl:length idx))
                              (adata (array-data a))
                              (value-index (first value-indexes))
                              )
  (declare (type fixnum axis value-index)
           (type cl:list idxs more-idxs value-indexes)
           (type cl:array adata))
  (flet ((setf-array-elements (i j)
           (declare (type fixnum i j))
           (let ((v-index
                  (tuple-sequence-number (mapcar #'vi-dim
                                                 (reverse (cons
                                                           (vi-info j
                                                                    idx-length)
                                                           value-indexes)))
                                         (mapcar #'vi-index
                                                 (reverse
                                                  (cons
                                                   (vi-info j idx-length)
                                                   value-indexes)))
                                         ))
                 )
             (declare (type fixnum v-index))
             #+silence!
             (format t ">>> setting at ~S ~S ~S ~S getting from ~S.~%"
                     (reverse (cons i idxs))
                     j
                     (reverse (cons j value-indexes))
                     (mapcar #'vi-dim
                             (reverse (cons
                                       (vi-info j idx-length)
                                       value-indexes)))
                     v-index
                     )
             (setf (apply #'aref adata (reverse (cons i idxs)))
                   ;; (aref value j)
                   (aref value v-index))))
         )
    (if (null more-idxs)
        (cond ((is-fixnum-vector idx)
               (locally
                 (declare (type (cl:vector fixnum) idx))
                 #'(lambda (arr)
                     (declare (type array arr) (ignore arr))
                     (loop for i across idx
                           for offset from 0
                           for j from 0 ; Dangerous
                           ;; for j of-type fixnum
                           ;; = value-index
                           ;; then (+ value-index (* offset idx-length))
                           #|
                           do (format t ">>> about to set ~S ~S (o: ~S vo: ~S).~%"
                                      i j offset value-offset)
                           |#
                           do (setf-array-elements i j)
                           finally (return value)
                           ))
                 ))

              ((is-real-vector idx)
               (locally
                 (declare (type (cl:vector real) idx))
                 #'(lambda (arr)
                     (declare (type array arr) (ignore arr))
                     (loop for i of-type real across idx
                           ;; for j of-type fixnum from 0 ; Dangerous
                           for j of-type fixnum from value-index
                           do (setf-array-elements (truncate i) j)
                           finally (return value)))
                 ))

              ((is-boolean-vector idx)
               ;; Need to fix 'recycling'.
               (locally
                 (declare (type (cl:vector boolean) idx))
                 #'(lambda (arr)
                     (declare (type array arr) (ignore arr))
                     (loop for i  of-type boolean across idx
                           for vi of-type fixnum from 0
                           ;; for j  of-type fixnum from 0 ; Dangerous!
                           for j of-type fixnum from value-index
                           when i
                           do (setf-array-elements vi j)
                           finally (return value)))
                 ))

              ((is-names-vector idx)
               (let ((axis-dimnames-pos (array-dimension-names-positions a axis)))
                 (declare (type (cl:vector (or string symbol)) idx)
                          (type hash-table axis-dimnames-pos))
                 #'(lambda (arr)
                     (declare (type array arr) (ignore arr))
                     (loop for n across idx
                           ;; for j of-type fixnum from 0 ; Dangerous!
                           for j of-type fixnum from value-index
                           do (setf-array-elements (gethash n axis-dimnames-pos) j)
                           finally (return value)))
                 ))

              (t (error "SETF INDEXING ERROR!"))
              )

        ;; There are MORE-IDXS.
        (let ((fmi (first more-idxs))
              (rmi (rest more-idxs))
              (ax1 (1+ axis))
              )
          (declare (type fixnum ax1))
          (cond ((is-fixnum-vector idx)
                 (locally
                   (declare (type (cl:vector fixnum) idx))
                   #'(lambda (a)
                       (declare (type array a))
                       (loop for i of-type fixnum across idx
                             ;; for j from 0 ; Dangerous
                             for j of-type fixnum
                             from 0
                             ;; from value-index ; by idx-length
                             for offset from 0
                             ;; do 
                             #+silence!
                             (format t ">> Iteration axis: ~S ~
                                           axis dim: ~S ~
                                           ids: ~S ~
                                           j: ~S ~
                                           offset: ~S ~
                                           value-indexes: ~S.~%"
                                        axis
                                        (array-dimension adata axis)
                                        (cons i idxs)
                                        j
                                        offset
                                        ;; (cons j value-indexes)
                                        (cons (vi-info j idx-length)
                                              value-indexes)
                                        )
                             do (funcall
                                 (setf-at-fc-access fmi ; (first more-idxs)
                                                    a
                                                    value
                                                    ax1 ; (1+ axis)
                                                    (cons i idxs)
                                                    rmi ; (rest more-idxs)
                                                    ;; (list j offset)
                                                    ;; (cons j value-indexes)
                                                    ;; The above should always be just (LIST J)
                                                    (cons (vi-info j idx-length)
                                                          value-indexes)
                                                    )
                                 a)
                             finally (return value)))
                   ))

                ((is-real-vector idx)
                 (locally
                   (declare (type (cl:vector real) idx))
                   #'(lambda (a)
                       (declare (type array a))
                       (loop for i of-type real across idx
                             ;; for j from 0
                             for j  of-type fixnum
                             from value-index ; by idx-length
                             do (funcall
                                 (setf-at-fc-access fmi ; (first more-idxs)
                                                    a
                                                    value
                                                    ax1 ; (1+ axis)
                                                    (cons (truncate i) idxs)
                                                    rmi ; (rest more-idxs)
                                                    (list j)
                                                    ;; j
                                                    )
                                 a)
                             finally (return value)))
                   ))

                ((is-boolean-vector idx)
                 (locally
                   (declare (type (cl:vector boolean) idx))
                   #'(lambda (a)
                       (declare (type array a))
                       (loop with j of-type fixnum = value-index
                             for i of-type boolean across idx
                             for ti of-type fixnum from 0
                             ;; for vj from 0 ; Check this, wrt J.
                             when i
                             do (incf j #|idx-length|#)
                             and do (funcall
                                     (setf-at-fc-access fmi ; (first more-idxs)
                                                        a
                                                        value
                                                        ax1 ; (1+ axis)
                                                        (cons ti idxs)
                                                        rmi ; (rest more-idxs)
                                                        ;; (list vj)
                                                        (list j)
                                                        ;; j
                                                        )
                                     a)
                             finally (return value)))
                   ))

                (t (error "SETF INDEXING ERROR (not implemented?)"))
                )
          ))
    ))


;;; (setf at) T ARRAY T
;;; -------------------

(defmethod (setf at) (v (a array) idx &rest more-idxs)
  (if (and (typep idx 'fixnum)
           (every #'(lambda (i) (typep i 'fixnum)) more-idxs))
      (setf (apply #'aref (array-data a) idx more-idxs) v)
      (funcall (setf-at-fc-access idx a v 0 () more-idxs) a)
      ))


;;; (setf at) T ARRAY MATRIX
;;; ------------------------
;;; Special method.

(defmethod (setf at) (v (a array) (m matrix) &rest more-indexes)
  ;; Each row of the matrix must be a proper index in the array.
  (assert (null more-indexes) (more-indexes)
    "Too many extra indexes when setting array by matrix index ~S."
    more-indexes)
  (loop with adata = (array-data a)
        with mdata = (matrix-data m)
        for i from 0 below (nrow m)
        do (loop for j from 0 below (ncol m)
                 collect (aref mdata i j) into index
                 finally (setf (apply #'aref adata index) v))
        finally (return v)))


;;; (setf at) VECTOR ARRAY MATRIX
;;; -----------------------------
;;; Special method.

(defmethod (setf at) ((v vector) (a array) (m matrix) &rest more-indexes)
  (assert (null more-indexes) (more-indexes)
    "Too many extra indexes when setting array by matrix index ~S."
    more-indexes)
  (loop with adata = (array-data a)
        with mdata = (matrix-data m)
        for i from 0 below (nrow m)
        do (loop for j from 0 below (ncol m)
                 collect (aref mdata i j) into index
                 finally (setf (apply #'aref adata index)
                               (at v i)))
        finally (return a)))


;;; (setf at) T ARRAY VECTOR
;;; ------------------------
;;; Special method.

(defmethod (setf at) (v (a array) (idx vector) &rest more-indexes)
  ;; In this case the indices in IDX are indices in the ROW-MAJOR
  ;; order.

  (if more-indexes
      (call-next-method)
      (let ((adata (array-data a))
            (idxdata (vector-data idx))
            )
        (loop for i across idxdata
              do (setf (row-major-aref adata i) v))
        v)))


;;; (setf at) VECTOR ARRAY VECTOR
;;; -----------------------------
;;; Special method for single VECTOR index.

(defmethod (setf at) ((v vector) (a array) (idx vector) &rest more-indexes)
  ;; In this case we replace the elemens of V in A the ROW-MAJOR order
  ;; as given by the content of IDX.

  (if more-indexes
      (call-next-method)
      (let ((adata (array-data a))
            (idxdata (vector-data idx))
            (vdata (vector-data v))
            )
        (assert (= (cl:length vdata) (cl:length idxdata)))
        (loop for i across idxdata
              for vel across vdata
              do (setf (row-major-aref adata i) vel))
        v)))

;;;; end of file -- array-indexing.lisp
