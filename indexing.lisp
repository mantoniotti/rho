;;;; -*- Mode: Lisp -*-

(in-package "RHO")

(defgeneric at (object index &rest indexes)
  (:documentation "The AT 'indexing' operation.")
  )


(define-condition indexing-error (error)
  ((obj :initarg :object
        ::reader indexing-error-object))
  (:documentation "The Indexing Error Condition.

This is the root of all the 'indexing' conditions possibly raised by
the RHO system."
   )
  )


(define-condition index-unknown (indexing-error)
  ((obj :reader index-unknown-object)
   (index :initarg :index
          :reader index-unknown-index)
   (indexes :initarg :indexes
            :reader index-unknown-indexes
            :initform ())
   )
  (:report (lambda (iu stream)
             (format stream
                     "Unknown index ~S in object ~S."
                     (index-unknown-index iu)
                     (index-unknown-object iu)))
   )
  (:documentation "The Index Unknown Error.

This error is signaled by the AT generic function whenever the
index(es) are undefined or 'out-of-bounds'.")
  )


(define-condition wrong-number-of-indices (indexing-error)
  ((obj-rank :initarg :indices-wanted
             :reader indices-wanted
             :type fixnum)
   (indices :initarg :indices-received
            :type cl:list
            :reader indices-received)
   )
  (:report (lambda (wnoi stream)
             (let ((obj (indexing-error-object wnoi))
                   (iw (indices-wanted wnoi))
                   (ir (indices-received wnoi))
                   )
               (format stream
                       "Indexing: ~A ~S~&wants ~D ~:[index~;indices~], ~
                        but ~D were given."
                       (type-of obj)
                       obj
                       iw
                       (> iw 1)
                       (cl:list-length ir)))
             )
   )
  (:documentation "The Wrong Number of Indices Error.

This error is signalled when an object is indexed with more subscripts
that is needs.  E.g. a VECTOR (a 1 rank object) indexed by more than
one subscript.")
  )


(defmethod at ((o t) index &rest indexes)
  (error 'index-unknown
         :object o
         :index index
         :indexes indexes))


;;; CLOS accessors.
;;; ===============

;;; In order to simplify the interface, we define two 'wrappers'.

(defstruct (reader (:conc-name the-)
                   (:constructor reader))
  (reader #'(lambda (o)
              (error 'index-unknown
                     :index :unknown-reader
                     :object o))
          :type (function (T) T)
          :read-only t))


(defstruct (writer (:conc-name the-)
                   (:constructor writer))
  (writer #'(lambda (v o)
              (declare (ignore v))
              (error 'index-unknown
                     :index :unknown-writer
                     :object o))
          :type (function (T T) T)
          :read-only t))


(defmethod at ((o standard-object) (index symbol) &rest indexes)
  "This method considers INDEX as either a reader or a slot name.

Exceptional Situations:

If INDEX is defined as a generic function, but it is not a reader for
the object O, then the results are undefined.  If INDEX is a plain
symbol, which does not name a slot in object O, then SLOT-VALUE will
signal an error.
"
  (declare (ignore indexes))
  (let ((sf (symbol-function index)))
    (if (and sf (typep sf 'generic-function))
        ;; We have made an educated guess that INDEX is a READER.
        (funcall sf o)
        ;; Otherwise, we call SLOT-VALUE.
        (slot-value o index))))


(defmethod at ((o standard-object) (index reader) &rest indexes)
  (declare (ignore indexes))
  (funcall (the-reader index) o))


(defmethod (setf at) (v (o standard-object) (index symbol) &rest indexes)
  "This method considers INDEX as a writer for STRUCTURE-OBJECT O.

Exceptional Situations:

If INDEX is defined as a function, but it is not a writer for
the object O, then the results are undefined. If INDEX is a plain
symbol, which does not name a slot in object O, then (SETF SLOT-VALUE)
will signal an error.
"
  (declare (ignore indexes))
  (let ((sf (symbol-function index)))
    (if (and sf (typep sf 'generic-function))
        ;; We have made an educated guess that INDEX is a WRITER.
        (funcall sf v o)
        ;; Otherwise, we call SLOT-VALUE.
        (setf (slot-value o index) v))))

(defmethod (setf at) (v (o standard-object) (index writer) &rest indexes)
  (declare (ignore indexes))
  (funcall (the-writer index) v o))


(defmethod at ((o structure-object) (index symbol) &rest indexes)
  "This method considers INDEX as a reader for STRUCTURE-OBJECT O.

Exceptional Situations:

If INDEX is defined as a function, but it is not a reader for
the object O, then the results are undefined. If INDEX is a plain
symbol, which may or may not name a slot in object O, then an error of
type INDEX-UNKNOWN is signalled.
"
  (declare (ignore indexes))
  (let ((sf (symbol-function index)))
    (if (and sf (typep sf 'function))
        ;; We have made an educated guess that INDEX is a READER.
        (funcall sf o)
        ;; Otherwise, we generate an error.
        (error 'index-unknown
               :object o
               :index index))))


(defmethod at ((o structure-object) (index reader) &rest indexes)
  (declare (ignore indexes))
  (funcall (the-reader index) o))


(defmethod (setf at) (v (o structure-object) (index symbol) &rest indexes)
  "This method considers INDEX as a writer for STRUCTURE-OBJECT O.

Exceptional Situations:

If INDEX is defined as a function, but it is not a writer for
the object O, then the results are undefined. If INDEX is a plain
symbol, which may or may not name a slot in object O, then an error of
type INDEX-UNKNOWN is signalled.
"
  (declare (ignore indexes))
  (let ((sf (symbol-function index)))
    (if (and sf (typep sf 'function)) 
        ;; We have made an educated guess that INDEX is a WRITER.
        (multiple-value-bind (vars vals store-vars writer-f)
            (get-setf-expansion (list index o))
          (progv ; Useful PROGV.
              (append vars store-vars)
              (append vals (list v))
            ;; Very expensive, but hey!
            (eval writer-f)))
        (error 'index-unknown
               :object o
               :index index))))

(defmethod (setf at) (v (o structure-object) (index writer) &rest indexes)
  (declare (ignore indexes))
  (funcall (the-writer index) v o))


;;; Standard AT and SETF methods on CL containers objects.
;;; ======================================================

;;; VECTORS and ARRAYS
;;; ------------------

(defmethod at ((v cl:vector) index &rest indices)
  (unless (null indices)
    (error 'wrong-number-of-indices
           :object v
           :indices-wanted 1
           :indices-received (cons index indices)))

  (aref v index))


(defmethod (setf at) (e (v cl:vector) index &rest indices)
  (unless (null indices)
    (error 'wrong-number-of-indices
           :object v
           :indices-wanted 1
           :indices-received (cons index indices)))

  (setf (aref v index) e))


(defmethod at ((a cl:array) index &rest indices)
  (when (/= (array-rank a)
            (1+ (cl:list-length indices)))
    (error 'wrong-number-of-indices
           :object a
           :indices-wanted (array-rank a)
           :indices-received (cons index indices)))

  (apply #'aref a index indices))


(defmethod (setf at) (e (a cl:array) index &rest indices)
  (when (/= (array-rank a)
            (1+ (cl:list-length indices)))
    (error 'wrong-number-of-indices
           :object a
           :indices-wanted (array-rank a)
           :indices-received (cons index indices)))

  (setf (apply #'aref a index indices) e))


;;; LISTs and CONSES
;;; ----------------

(defmethod at ((l cl:list) index &rest indices)
  (unless (null indices)
    (error 'wrong-number-of-indices
           :object l
           :indices-wanted 1
           :indices-received (cons index indices)))
  (nth index l))


(defmethod (setf at) (e (l cl:list) index &rest indices)
  (unless (null indices)
    (error 'wrong-number-of-indices
           :object l
           :indices-wanted 1
           :indices-received (cons index indices)))
  (setf (nth index l) e))


;;; Some specialized methods.

(defmethod at ((c cl:cons) (index (eql 'first)) &rest indices)
  (unless (null indices)
    (error 'wrong-number-of-indices
           :object c
           :indices-wanted 1
           :indices-received (cons index indices)))
  (car c))


(defmethod (setf at) (e (c cl:cons) (index (eql 'first)) &rest indices)
  (unless (null indices)
    (error 'wrong-number-of-indices
           :object c
           :indices-wanted 1
           :indices-received (cons index indices)))
  (setf (car c) e))


(defmethod at ((c cl:cons) (index (eql 'rest)) &rest indices)
  (unless (null indices)
    (error 'wrong-number-of-indices
           :object c
           :indices-wanted 1
           :indices-received (cons index indices)))
  (cdr c))


(defmethod (setf at) (e (c cl:cons) (index (eql 'rest)) &rest indices)
  (unless (null indices)
    (error 'wrong-number-of-indices
           :object c
           :indices-wanted 1
           :indices-received (cons index indices)))
  (setf (cdr c) e))


;;; HASH-TABLES
;;; -----------

(defmethod at ((ht cl:hash-table) index &rest indices)
  (let ((li (list-length indices)))
    (if (> li 1)
        (error 'wrong-number-of-indices
           :object l
           :indices-wanted 1
           :indices-received (cons index indices))

        ;; INDICES is empty or it contains the GETHASH default; kludgy
        ;; but effective.
        (apply #'gethash ht index indices))))


(defmethod (setf at) (e (ht cl:hash-table) index &rest indices)
  (unless (null indices)
    (error 'wrong-number-of-indices
           :object l
           :indices-wanted 1
           :indices-received (cons index indices)))
  (setf (gethash ht index) e))
        
;;;; end of file -- indexing.lisp
