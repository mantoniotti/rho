;;;; -*- Mode: Lisp -*-

;;;; array-predicates.lisp --
;;;; Array (and matrices and vectors) useful predicates are
;;;; collected here.

(in-package "RHO")

;;; Array Predicates.
;;; =================

(defun is-fixnum-vector (vec
                         &aux
                         (v (etypecase vec
                              (cl:vector vec)
                              (vector (vector-data vec)))))
  (declare (type (or cl:vector vector) vec)
           (type cl:vector v))
  (or (subtypep (cl:array-element-type v) 'fixnum)
      (every #'(lambda (e)
                 (typecase e
                   (fixnum t)
                   (integer
                    (<= most-negative-fixnum e most-positive-fixnum))))
             v)))


(defun is-boolean-vector (vec
                          &aux
                          (v (etypecase vec
                               (cl:vector vec)
                               (vector (vector-data vec)))))
  (declare (type (or cl:vector vector) vec)
           (type cl:vector v))
  (or (subtypep (cl:array-element-type v) 'boolean)
      (every #'(lambda (e)
                 (or (eq e t)
                     (eq e nil)))
             v)))


(defun is-vector-with-element-type (vec type
                                        &aux
                                        (v (etypecase vec
                                             (cl:vector vec)
                                             (vector (vector-data vec)))))
  (declare (type (or cl:vector vector) vec)
           (type cl:vector v))
  (or (subtypep (cl:array-element-type v) type)
      (every #'(lambda (e) (typep e type)) v)))


(defun is-real-vector (vec)
  (declare (type (or cl:vector vector) vec))
  (is-vector-with-element-type vec 'real))


(defun is-names-vector (vec)
  (declare (type (or cl:vector vector) vec))
  (is-vector-with-element-type vec '(or string symbol)))


(defgeneric is-array-with-element-type (a type)
  (:documentation "Checks whether the array A has elements of type TYPE.

The methods for this generic function are defined also for subclasses
of ARRAY."))


(defmethod is-array-with-element-type ((a cl:array) type)
  (or (subtypep (cl:array-element-type a) type)
      (let ((va (make-array (cl:array-total-size a)
                            :displaced-to a
                            :element-type (cl:array-element-type a)))
            )
        (every #'(lambda (e) (typep e type)) va))))


(defmethod is-array-with-element-type ((a array) type)
  (is-array-with-element-type (array-data a) type))


(defmethod is-array-with-element-type ((v cl:vector) type)
  (or (subtypep (cl:array-element-type v) type)
      (every #'(lambda (e) (typep e type)) v)))


(defmethod is-array-with-element-type ((v vector) type)
  (is-array-with-element-type (vector-data v) type))


;;;; end of file -- array-predicates.lisp
