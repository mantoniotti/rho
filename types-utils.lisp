;;;; -*- Mode: Lisp -*-

;;;; types-utils.lisp

(in-package "RHO")

(defgeneric is-container (c)
  (:method ((c cl:t)) nil)
  (:method ((c cl:null)) nil)
  (:method ((c cl:array)) t)
  (:method ((c cl:sequence)) t)
  (:method ((c cl:structure-object)) t)
  (:method ((c cl:standard-object)) t)
  (:documentation
   "Checks whether C is a data structure that 'contains' something.")
  )


(defun containerp (c)
  "Checks whether C is a data structure that 'contains' something.

It is a synonim of IS-CONTAINER.

See Also:

IS-CONTAINER.
"
  (is-container c))



(defgeneric element-type (c &optional recursive)
  (:documentation "Guesses the 'element type' of a container object."))


(defmethod element-type ((obj t) &optional recursive)
  (declare (ignore recursive))
  'null) ; Kitchen sink case for 'atomic' objects.

(defmethod element-type ((s sequence) &optional recursive)
  (declare (ignore recursive))
  t) ; Kitchen sink case for SEQUENCEs.

(defmethod element-type ((a cl:array) &optional recursive)
  (declare (ignore recursive))
  (cl:array-element-type a))

(defmethod element-type ((l cl:list) &optional recursive) ; A proper list.
  (if (null l)
      'null
      (flet ((list-item-type-of (e)
               (if (and (not (atom e)) recursive)
                   (element-type e recursive)
                   (type-of e)))
             )
        (loop with list-et = (list-item-type-of (first l))
              for e in l
              for et = (list-item-type-of e)
              do (multiple-value-bind (et-is-subtype sure)
                     (subtypep et list-et)
                   (cond ((and et-is-subtype sure) #| no-op |#)

                         ((and (null et-is-subtype) sure)
                          (multiple-value-bind (list-et-is-subtype sure)
                              (subtypep list-et et)
                            (cond ((and list-et-is-subtype sure)
                                   (setf list-et et))
                                  ((and (null list-et-is-subtype) sure)
                                   (setf list-et `(or ,list-et ,et)))
                                  ((and (null list-et-is-subtype) (null sure))
                                   (warn "Inconsistent types.")
                                   (setf list-et t))
                                  (t
                                   (warn "Something strange happened: (SUBTYPEP ~S ~S) ==> T, NIL.")
                                   (setf list-et t)))))
                       
                         ((and (null et-is-subtype) (null sure))
                          (warn "Non recognizable subtypes.")
                          (setf list-et t)
                          )
                       
                         (t
                          (warn "Something strange happened: (SUBTYPEP ~S ~S) ==> T, NIL.")
                          (setf list-et t))
                         ))
              finally (return (simplify-or-type list-et))
              ))))


(defun simplify-or-type (disj-type)
  ;; Special function to be used only in the above.
  ;; case where OR types are built in a specific order.
  (if (is-or-type disj-type)
      (destructuring-bind (or-kwd dt1 dt2)
          disj-type
        (declare (ignore or-kwd))
        (let ((simplified-disj-dt1 (simplify-or-type dt1)))
          (if (is-or-type simplified-disj-dt1)
              `(or ,@(rest simplified-disj-dt1) ,dt2)
              `(or ,simplified-disj-dt1 ,dt2))))
      disj-type))


(defun is-or-type (type-spec)
  ;; Ok.  This is quite bad!  I should check that it actually is a
  ;; type-spec.
  (and (consp type-spec) (eq 'or (first type-spec))))


;;;; end of file -- types-utils.lisp
