;;;; -*- Mode: Lisp -*-

;;;; robj.lisp --
;;;; The main issue with R basic structured objects (cfr. vectors) is
;;;; that they have 'names' for their components.
;;;; Either we wrap everything (the choice made) or we rely on
;;;; "external" tables keyed by the object themselves.  This second
;;;; choice looks more expensive, given the status of CL EQUALP hash
;;;; tables

(in-package "RHO")


;;; robj
;;; ----
;;; The root of the - DEFSTRUCT based - obj hierarchy.
;;; Each object may have a 'name' and 'names' for its 'components'

(defstruct (robj (:constructor nil))
  (name nil :type (or null symbol string))
  (names () :type cl:sequence)
  (names-pos nil :type (or null hash-table))
  )


;;; name
;;; ----
;;; Accessing the name of a ROBJ.

(defun name (robj)
  (declare (type robj robj))
  (robj-name robj))


(defun (setf name) (n robj)
  (declare (type robj robj)
           (type (or null symbol string) n))
  (setf (robj-name robj) n))


;;; init-names
;;; ----------
;;; An auxiliary function used to initialize the 'names' slot of
;;; several objects.

(defun init-names (names &optional (nn 0 nnp))
  ;; Either NAMES is NIL or it is a list of (OR SYMBOL STRING)
  (declare (type fixnum nn)
           (type boolean nnp)
           (type list names))
  (when names
    (when nnp
      (assert (= nn (length names))))

    (let ((ns (make-hash-table :test #'equal)))
      (loop for n in names
            for i from 0
            for ni = (gethash n ns)
            unless ni
            do (setf (gethash n ns) i)
            finally (return ns)))
    ))


;;; names
;;; -----

(defgeneric names (robj)
  (:documentation
   "Returns a list of names associated to ROBJ.")

  (:method ((robj robj))
   (robj-names robj))
  )


#| Vectors and arrays need different treatment.

(defmethod names :after ((robj robj))
  (unless (robj-names-pos robj)
    (let ((nps-ht (init-names (robj-names robj))))
      (setf (robj-names-pos robj) nps-ht))))

|#


;;;; end of file -- robj.lisp
