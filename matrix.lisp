;;;; -*- Mode: Lisp -*-

;;;; matrix.lisp --
;;;; The main issue with R basic structured objects (cfr. arrays) is
;;;; that they have 'names' for their components.
;;;; Here we define MATRIXes. Cfr., file 'robj.lisp' to understand the
;;;; wrapping.

(in-package "RHO")


;;;; Prologue
;;;; ========

(defgeneric ncol (rxc-obj))
(defgeneric nrow (rxc-obj))


;;; matrix

(defstruct (matrix (:include array)
                   (:constructor matrix
                    (initial-contents
                     &key
                     name
                     (splice nil) ; If SPLICE is T, a, say, VECTOR
                                  ; INITIAL-CONTENTS is used as the
                                  ; element of each entry in the matrix;
                                  ; if NIL the vector elements are used as
                                  ; elements of the matrix (depending on
                                  ; the FILL parameter).
                     (fill t)

                     (nrow (or (and (null splice)
                                    (> (rank initial-contents) 1)
                                    (first (dimensions initial-contents t)))
                               1))
                     (ncol (or (and (null splice)
                                    (second (dimensions initial-contents t)))
                               1))
                     (dimnames () dimnames-supplied-p)

                     (element-type
                      (if (or splice (not (is-container initial-contents)))
                          (type-of initial-contents)
                          (element-type initial-contents t)))

                     &aux
                     (dimensions (list nrow ncol))

                     (dimension-names
                      (if (and dimnames-supplied-p
                               (= (cl:length dimnames)
                                  (cl:length dimensions)))
                          dimnames
                          (when dimnames
                            (warn "Dimension names ~S incompatibles with dimensions ~S."
                                  dimnames
                                  dimensions))))

                     (dimension-names-pos (list nil nil))

                     (data
                      (shape 'cl:array
                             (if splice
                                 (cl:vector initial-contents)
                                 initial-contents)
                             (list nrow ncol)
                             :element-type element-type
                             :check-total-size nil
                             :fill fill)))
                    )
                  )
  (nrow 1 :type (integer 0 (#.most-positive-fixnum)))
  (ncol 1 :type (integer 0 (#.most-positive-fixnum)))
  )


(defstruct (row (:include vector)
                (:constructor row*
                 (initial-contents
                  &key
                  names
                  name
                  (size (cl:length initial-contents))
                  (element-type (element-type initial-contents))
                  &aux
                  (data
                   (shape 'cl:vector
                          initial-contents
                          size
                          :element-type element-type
                          :check-total-size nil))))


                ))

(defstruct (column (:include vector)
                   (:constructor column*
                    (initial-contents
                     &key
                     names
                     name
                     (size (cl:length initial-contents))
                     (element-type (element-type initial-contents))
                     &aux
                     (data
                      (shape 'cl:vector
                             initial-contents
                             size
                             :element-type element-type
                             :check-total-size nil))))
                   ))


;;; diag

(defgeneric diag (m &key raw)
  (:method ((a cl:array) &key (raw nil))
   (declare (type boolean raw))
   (assert (= 2 (array-rank a)))
   (let* ((dd (min (array-dimension a 0) (array-dimension a 1)))
          (d (make-array dd
                         :element-type (array-element-type a)
                         :initial-element (aref a 0 0)))
          )
     (declare (type fixnum dd))
     (flet ((result ()
              (if raw
                  d
                  (vector* :size dd
                           :initial-contents d
                           :element-type (array-element-type d))))
            )
       (dotimes (i dd (result))
         (dotimes (j dd)
           (setf (aref d i) (aref a i j)))))))

  (:method ((m matrix) &key (raw nil))
   (declare (type boolean raw))
   (let* ((a (matrix-data m))
          (dd (min (array-dimension a 0) (array-dimension a 1)))
          (d (make-array dd
                         :element-type (array-element-type a)
                         :initial-element (aref a 0 0)))
          )
     (declare (type fixnum dd))
     (flet ((result ()
              (if raw
                  d
                  (vector* :size dd
                           :initial-contents d
                           :element-type (array-element-type d))))
            )
       (dotimes (i dd (result))
         (dotimes (j dd)
           (setf (aref d i) (aref a i j)))))
     ))
  )


(defgeneric (setf diag) (v m)
  (:method (v (a cl:array))
   (assert (= 2 (array-rank a)))
   (let ((dd (min (array-dimension a 0) (array-dimension a 1))))
     (declare (type fixnum dd))
     (set-matrix-diagonal a dd v)))

  (:method (v (m matrix))
   (let* ((a (matrix-data m))
          (dd (min (array-dimension a 0) (array-dimension a 1)))
          )
     (declare (type fixnum dd))
     (set-matrix-diagonal a dd v)
     m))
  )


(defun set-matrix-diagonal (m d dv)
  (dotimes (i d m)
    (setf (aref m i i) dv)))


;;; id

(defun id (n &optional (element-type 'fixnum))
  (let* ((e (coerce 0 element-type))
         (idm (matrix e :nrow n :ncol n :element-type element-type))
         )
    (set-matrix-diagonal (matrix-data idm) n (coerce 1 element-type))
    idm
    ))


;;; ncol

(defmethod ncol ((a array))
  (second (dimensions a)))

(defmethod ncol ((m matrix))
  (matrix-ncol m))


;;; nrow

(defmethod nrow ((a array))
  (first (dimensions a)))

(defmethod nrow ((m matrix))
  (matrix-nrow m))


;;; colnames

(defgeneric colnames (m &key do-null prefix)
  (:method ((a array) &key (do-null t) (prefix "col"))
   (let* ((ds (dimensions (array-data a)))
          (ncols (second ds))
          (cns (second (array-dimension-names a)))
          (cnsl (length cns))
          )
     (cond (cns
            (cond ((= cnsl ncols) cns)
                  ((> cnsl ncols) (subseq cns 0 ncols))
                  (t
                   (if (null do-null)
                       (concatenate 'list
                                    cns
                                    (loop for i from 0 below (- ncols cnsl)
                                          collect (format nil "~A~D"
                                                          prefix
                                                          (+ cnsl i))))
                       cns))
                  ))
           ((null do-null)
            (loop for i from 0 below ncols
                  collect (format nil "~A~D"
                                  prefix
                                  i)))
           )))
  )


;;; rownames

(defgeneric rownames (m &key do-null prefix)
  (:method ((a array) &key (do-null t) (prefix "row"))
   (let* ((ds (dimensions (array-data a)))
          (nrows (first ds))
          (rns (first (array-dimension-names a)))
          (rnsl (length rns))
          )
     (cond (rns
            (cond ((= rnsl nrows) rns)
                  ((> rnsl nrows) (subseq rns 0 nrows))
                  (t
                   (if (null do-null)
                       (concatenate 'list
                                    rns
                                    (loop for i from 0 below (- nrows rnsl)
                                          collect (format nil "~A~D"
                                                          prefix
                                                          (+ rnsl i))))
                       rns))
                  ))
           ((null do-null)
            (loop for i from 0 below nrows
                  collect (format nil "~A~D"
                                  prefix
                                  i)))
           )))
  )


#|
(defun row (m i
              &aux
              (rdata (matrix-data m))
              (cols (ncol m)))
  (declare (type matrix m)
           (type fixnum i cols))
  (make-array cols
              :element-type (array-element-type rdata)
              :displaced-to rdata
              :displaced-index-offset (* i cols))
  )
|#


(declaim (ftype (function (matrix fixnum &key (:raw boolean))
                          (or cl:vector row))
                row))

(declaim (ftype (function (matrix fixnum &key (:raw boolean))
                          (or cl:vector column))
                col))


(defun row (m i
              &key
              (raw t)
              &aux
              ;; (rdata (matrix-data m))
              (rdata (array-data m)) ; General.
              (cols (ncol m)))
  (declare ; (type matrix m)
   
   (type fixnum i cols))
  (let ((raw-row
         (make-array cols
                     :element-type (cl:array-element-type rdata)
                     :initial-contents (loop for j from 0 below cols
                                             collect (aref rdata i j))))
        )
    (if raw
        raw-row
        (row* raw-row :size cols)
        )))


(defun col (m j
              &key
              (raw t)
              &aux
              ;; (rdata (matrix-data m))
              (rdata (array-data m)) ; General.
              (rows (nrow m)))
  (declare ; (type matrix m)
   (type fixnum j rows))
  (let ((raw-col
         (make-array rows
                     :element-type (cl:array-element-type rdata)
                     :initial-contents (loop for i from 0 below rows
                                             collect (aref rdata i j))))
        )
    (if raw
        raw-col
        (column* raw-col :size rows)
        )))




;; #+array-non-destructive
(defmethod (setf dimensions) (dims (a matrix) &optional recursive)
  (declare (ignorable recursive)
           (type list dims))
  (matrix (as-vector a) :nrow (first dims) :ncol (second dims)))


#+array-destructive
(defmethod (setf dimensions) (dims (a array) &optional recursive)
  (declare (ignorable recursive)
           (type list dims))
  (setf (array-data a)
        (shape 'cl:array (as-vector a) dims))

  dims)

;;; at

#|
(defmethod at ((m matrix) r &rest c-etc)
  (let ((c (first c-etc)))
    ;; Simple FTTB.
    (aref (matrix-data m) r c)))
|#

(defmethod at ((m matrix) r &rest c-etc)
  (let ((c (first c-etc)))
    (cond ((and r c)
           (aref (matrix-data m) r c))
          (r
           (row m r :raw nil))
          (c
           (col m c :raw nil))
          (t m) ; Not quite as there is a ARRAY NULL method.
          )))


(defmethod at ((m matrix) (r null) &rest c-etc)
  (let ((c (first c-etc)))
    (if c
        (col m c :raw nil)
        m
        )))


#|
(defmethod (setf at) (v (m matrix) r &rest c-etc)
  (let ((c (first c-etc)))
    ;; Simple FTTB.
    (setf (aref (matrix-data m) r c) v)))
|#

;;; Special matrix based addressing of matrices and arrays.

(defmethod at ((a array) (m matrix) &rest more-indexes)
  (declare (ignore more-indexes))
  (loop for r from 0 below (nrow m)
        collect (at a (the cl:vector (row m r :raw t))) into result
        finally (return (apply #'vector result))
        ))
  

;;;; end of file -- matrix.lisp
