;;;; -*- Mode: Lisp -*-

;;;; printing.lisp
;;;; Basic definitions related to "printing" in RHO.

(in-package "RHO")

(defparameter *print-rows* t
  "Controls the printing of 'rows' in arrays etc.

If the value is non null and a REAL, then at most that many rows
(truncated) are printed. If the value if non null or T (the default)
then all rows are printed. If the value is NIL, then no rows are
printed.")


(defparameter *print-cols* t
  "Controls the printing of 'cols' in arrays etc.

If the value is non null and a REAL, then at most that many cols
(truncated) are printed. If the value if non null or T (the default)
then all cols are printed. If the value is NIL, then no cols are
printed.")


(defparameter *print-dimensions* t
  "Controls the printing of 'dimensions' in arrays etc.

If the value is non null and a REAL, then at most that many dimensions
(truncated) are printed. If the value if non null or T (the default)
then all dimensions are printed. If the value is NIL, then no
dimensions are printed.")


(defparameter *print-dimension-names* t
  "Controls the printing of 'dimension names' in arrays.

If non null or T (the default) all dimension names are printed.  If
the value is NIL then dimension names are not printed.")


(defparameter *print-displaying* t
  "Controls whether the printing of vectors and matrices is 'displayed'.

If non null or T (the default), grid-like data structures are printed
using a (human readable) tabular format.")


#|
(defgeneric print-sequence (seq &optional stream)
  (:method ((seq cl:sequence) &optional (stream *standard-output*))
   (let* ((sl (cl:length seq))
          (n (min (or *print-length* most-positive-fixnum) sl))
          )
     (write-string "[" stream)
     (when (plusp sl)
       (format stream "~A" (elt seq 0)))
     (loop for i from 1 below n
           do (format stream ", ~A" (elt seq i))
           finally
           (if (< n sl)
               (write-string ", ...]" stream)
               (write-string "]" stream))
           (finish-output stream)
           )))
  )
|#

(defgeneric print-sequence (seq &optional stream)
  (:method ((seq cl:sequence) &optional (stream *standard-output*))
   (let* ((sl (cl:length seq))
          (n (min (or *print-length* most-positive-fixnum) sl))
          )
     (when (plusp sl)
       ;; (format stream "~A" (elt seq 0))
       (princ (elt seq 0))
       )
     (loop for i from 1 below n
           do ;; (format stream " ~A" (elt seq i))
           (write-char #\Space stream)
           (princ (elt seq i))
           finally
           (when (< n sl)
             (write-string " ..." stream))
           (finish-output stream)
           (return seq)
           )))
  )
        
  

;;;; end of file -- printing.lisp
