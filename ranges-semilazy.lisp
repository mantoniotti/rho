;;;; -*- Mode: Lisp -*-

;;;; ranges.lisp

(in-package "RHO")

(defstruct (range (:include vector)
                  (:constructor range (&optional
                                       (from 0)
                                       to
                                       (by 1))))
  (from 0 :type real :read-only t)
  (to nil :type (or null real) :read-only t)
  (by 1 :type real :read-only t)
  (is-filled nil :type boolean)
  )


(defun neg-range (&optional (from 0) to (by -1))
  (range from to by))


(defun fill-range (r i)
  (declare (type range r))
  (let ((from (range-from r))
        (to (range-to r))
        (by (range-by r))
        )
    (declare (type real from by)
             (type (or null real) to))
    
    (etypecase i
      (fixnum )
      (vector )
      (cl:vector ))
    ))


(defmethod at :before ((r range) i &rest more-indices)
  (declare (ignore more-indices))
  (let ((rdata (range-data r)))
    (let ((rfrom (range-from r))
          (rto (range-to r))
          (rby (range-by r))
          )

      (if (range-is-filled r)
          (if rto
              (aref rdata i)
              (let ((ld (cl:length rdata)))
                (if ))))
              
              
            
        (loop for i from from to to by by
              collect 
        ))))


(defmethod at ((r range) i &rest more-indexes)
  (declare (ignore more-indexes))
  (let ((from (range-from r))
        (to (range-to r))
        (by (range-by r))
        (i (the fixnum
                (etypecase i
                  (fixnum i)
                  (real (truncate i)))))
        )
    (declare (type real from by)
             (type (or null real) to)
             (type fixnum i))
    (let ((result (+ (* by i) from)))
      (if to
          (if (>= to result)
              result
              (error "Out of bounds."))
          result))))


;;; More AT methods.

(defmethod at ((v vector) (r range) &rest more-indexes)
  (declare (ignore more-indexes))
  (let ((from (range-from r))
        (to (range-from r))
        (by (range-by r))
        )
    (loop for i from 
  )




;;;; end of file -- ranges.lisp
