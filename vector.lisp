;;;; -*- Mode: Lisp -*-

;;;; vector.lisp --
;;;; The main issue with R basic structured objects (cfr. vectors) is
;;;; that they have 'names' for their components.
;;;; Here we define VECTORs. Cfr., file 'robj.lisp' to understand the
;;;; wrapping.

(in-package "RHO")

;;; vector

(defstruct (vector (:include array
                    (data #() :type cl:vector :read-only t))

                   (:constructor vector*
                    (initial-contents
                     &key
                     (names () names-supplied-p)
                     name
                     (size (cl:length initial-contents))
                     (element-type (element-type initial-contents))
                     &aux
                     (dimensions (list size))

                     (dimension-names (and names-supplied-p (list names)))

                     (dimension-names-pos (make-list 1))

                     (data
                      (shape 'cl:vector
                             initial-contents
                             size
                             :element-type element-type
                             :check-total-size nil))))

                   (:constructor vector
                    (&rest
                     initial-elements
                     &aux
                     (size (cl:list-length initial-elements))

                     (dimensions (list size))

                     (dimension-names nil)

                     (dimension-names-pos (make-list 1))

                     (data (shape 'cl:vector
                                  initial-elements
                                  (cl:list-length initial-elements)
                                  :element-type (element-type initial-elements)
                                  :check-total-size nil))))
                   ))


#|
(defmethod print-object ((v vector) stream)
  (cond (*print-readably*
         (call-next-method))

        (*print-displaying*
         (let* ((vname (vector-name v))
                (vnl (cl:length (string vname)))
                (names (or (and (dimension-names v)
                                (first (dimension-names v)))
                           (vector-names v)))
                (longest-name (loop for n in names
                                    maximize (cl:length (string n))))
                )
           (format stream "~:[~2*~;~VA~%~]~@[~:{  ~V@A~}~%~]~:[[]~;~:*~:{  ~V@A~}~]~%"
                   vname
                   vnl
                   vname
                   (mapcar (lambda (n)
                             (list longest-name n))
                           names)
                   (map 'list (lambda (v)
                                (list longest-name v))
                        (vector-data v)))
           )
         )

        (t
         (print-unreadable-object (v stream :type t)
           (when (vector-name v)
             (format stream "~A : " (vector-name v)))
           ;; (format stream "~S" (vector-data v))
           (write-char #\[ stream)
           (print-sequence (vector-data v) stream)
           (write-char #\] stream)
           )))
  )
|#

;;; names
;;; -----

(defmethod names :after ((v vector))
  (unless (vector-names-pos v)
    (let ((nps-ht (init-names (vector-names v))))
      (setf (vector-names-pos v) nps-ht))))


;;; at
;;; --

(defmethod at ((v vector) i &rest indexes)
  (declare (ignore indexes))
  (aref (vector-data v) i))


(defmethod (setf at) (val (v vector) i &rest indexes)
  (declare (ignore indexes))
  (setf (aref (vector-data v) i) val))





(defmethod at ((vec vector) (index vector) &rest indexes)
  (declare (ignore indexes))
  (let ((v (vector-data vec))
        (i (vector-data index))
        )
    (declare (type cl:vector v i))

    (cond ((is-fixnum-vector index)
           (extract-fixunm-indexed-elements vec v index i))

          ((is-real-vector index)
           (let ((i (map '(vector fixnum) #'truncate i)))
             (extract-fixunm-indexed-elements vec v index i)))

          ((is-boolean-vector index)
           (extract-boolean-indexed-elements vec v index i))

          ((is-names-vector index)
           (extract-name-indexed-elements vec v index i))

          (t (warn "Not yet implemented.") nil)))
  )


(defmethod at ((vec vector) (index cl:vector) &rest indexes)
  (declare (ignore indexes))
  (let ((v (vector-data vec))
        (i index)
        )
    (declare (type cl:vector v i))

    (cond ((is-fixnum-vector i)
           (extract-fixunm-indexed-elements vec v index i))

          ((is-real-vector index)
           (let ((i (map '(vector fixnum) #'truncate i)))
             (extract-fixunm-indexed-elements vec v index i)))

          ((is-boolean-vector i)
           (extract-boolean-indexed-elements vec v index i))

          ((is-names-vector i)
           (extract-name-indexed-elements vec v index i))

          (t (warn "Not yet implemented.") nil)))
  )


(defmethod at ((vec vector) (index cl:list) &rest indexes)
  (apply #'at vec (coerce index 'cl:vector) indexes))


;;;; end of file -- vector.lisp
