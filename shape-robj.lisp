;;;; -*- Mode: Lisp -*-

;;;; shape-robj.lisp --
;;;; Specialized methods for SHAPEing R-OBJS such as ARRAYs and
;;;; VECTORs etc. etc.

;;;; See file COPYING for copyright and license information.

(in-package "RHO")


;;;; Implementation
;;;; --------------

;;; shape methods

(defmethod shape ((result-type (eql 'cl:vector))
                  (v rho:array)
                  dimensions
                  &key
                  (by-row t)
                  (element-type (element-type v) etsp)
                  (check-total-size t)
                  (fill t)
                  &allow-other-keys
                  )
  (shape 'cl:vector
         (array-data v)
         dimensions
         :by-row by-row
         :element-type element-type
         :check-total-size check-total-size
         :fill fill))


(defmethod shape ((result-type (eql 'cl:array))
                  (v rho:array)
                  dimensions
                  &key
                  (by-row t)
                  (element-type (element-type v) etsp)
                  (check-total-size t)
                  (fill t)
                  &allow-other-keys
                  )
  (shape 'cl:array
         (array-data v)
         dimensions
         :by-row by-row
         :element-type element-type
         :check-total-size check-total-size
         :fill fill))


(defmethod shape ((result-type (eql 'cl:list))
                  (v rho:array)
                  dimensions
                  &key
                  (by-row t)
                  (element-type (element-type v) etsp)
                  (check-total-size t)
                  (fill t)
                  &allow-other-keys
                  )
  (shape 'cl:list
         (array-data v)
         dimensions
         :by-row by-row
         :element-type element-type
         :check-total-size check-total-size
         :fill fill))

;;;; end of file -- shape-robj.lisp
