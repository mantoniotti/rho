;;;; -*- Mode: Lisp -*-

;;;; array.lisp --
;;;; The main issue with R basic structured objects (cfr. arrays) is
;;;; that they have 'names' for their components.
;;;; Here we define ARRAYs. Cfr., file 'robj.lisp' to understand the
;;;; wrapping.

(in-package "RHO")


;;;; Prologue
;;;; ========

;;; array

(defstruct (array (:include robj)
                  (:constructor array
                   (initial-contents
                    &key
                    name
                    (splice nil) ; If SPLICE is T, a, say, VECTOR
                                 ; INITIAL-CONTENTS is used as the
                                 ; element of each entry in the matrix;
                                 ; if NIL the vector elements are used as
                                 ; elements of the matrix (depending on
                                 ; the FILL parameter).
                    (fill t)

                    (dim (if splice (list 1) (dimensions initial-contents t)))
                    (dimnames () dimnames-supplied-p)

                    (element-type
                     (if splice
                         (type-of initial-contents)
                         (element-type initial-contents t)))

                    &aux
                    (dimensions
                     (typecase dim
                       (fixnum (list dim))
                       (list dim)
                       (t (error "~S is an illegal array dimension."
                                 dim))))


                    (dimension-names
                     (if (and dimnames-supplied-p
                              (= (cl:length dimnames)
                                 (cl:length dimensions)))
                         dimnames
                         (when dimnames
                           (warn "Dimension names ~S incompatibles with dimensions ~S."
                                 dimnames
                                 dim))))

                    (dimension-names-pos (make-list (cl:length dimensions)))

                    (data
                     (shape 'cl:array (if splice
                                          (cl:vector initial-contents)
                                          initial-contents)
                            dimensions
                            :element-type element-type
                            :check-total-size nil
                            :fill fill)))
                   )
                  )
  "The ARRAY Structure Class."
  (element-type t :read-only t) ; Cannot quite trust UPGRADED-ARRAY-ELEMENT-TYPE.

  (dimension-names () :type cl:sequence)

  (dimension-names-pos () :type cl:sequence)

  (data #()
        :type cl:array
        :read-only t))


(defmethod is-scalar ((a array)) nil)


(defmethod element-type ((a array) &optional recursive)
  (declare (ignore recursive))
  (array-element-type a))


(defmethod dimensions ((a array) &optional recursive)
  (declare (ignorable recursive))
  (cl:array-dimensions (array-data a)))


;; #+array-non-destructive
(defmethod (setf dimensions) (dims (a array) &optional recursive)
  (declare (ignorable recursive)
           (type list dims))
  (array (as-vector a) :dim dims))


#+array-destructive
(defmethod (setf dimensions) (dims (a array) &optional recursive)
  (declare (ignorable recursive)
           (type list dims))
  (setf (array-data a)
        (shape 'cl:array (as-vector a) dims))

  dims)


(defgeneric dimension-names (a &optional axis)
  (:method ((a array) &optional axis)
   (declare (type (or null fixnum) axis))
   (let ((adn (array-dimension-names a)))
     (if (and axis adn)
         (elt adn axis)
         adn))))


(defgeneric (setf dimension-names) (names a &optional axis)
  (:method (names (a array) &optional axis)
   (if axis
       (setf (elt (array-dimension-names-pos a) axis)
             (init-names names axis)

             (elt (array-dimension-names a) axis)
             names)
       (let ((anps (array-dimension-names-pos a)))
         (loop for ns in names
               for ns-axis from 0
               do (setf (elt anps ns-axis) (init-names ns)))
         (setf (array-dimension-names a) names)))))


#| Useless
(defun array-names-positions (a &optional axis)
  (declare (type array a)
           (type (or null fixnum) axis))
  (let ((anps (array-names-pos a)))
    (if axis
        (let ((anpsa (elt anps axis)))
          (if anpsa
              anpsa
              (setf (elt anps axis) (init-names (dimension-names a axis)))))
        (loop for ns in (dimension-names a)
              for ns-axis from 0
              do (setf (elt anps ns-axis) (init-names ns))
              finally (return anps)))
    ))
|#


(defun array-dimension-names-positions (a &optional axis)
  (declare (type array a)
           (type (or null fixnum) axis))
  (let ((anps (array-dimension-names-pos a)))
    (if axis
        (let ((anpsa (elt anps axis)))
          (if anpsa
              anpsa
              (setf (elt anps axis) (init-names (dimension-names a axis)))))
        (loop for ns in (dimension-names a)
              for ns-axis from 0
              do (setf (elt anps ns-axis) (init-names ns))
              finally (return anps)))
    ))


(defmethod rank ((a array) &optional recursive)
  (declare (ignore recursive))
  (cl:array-rank (array-data a)))


#| This is wrong w.r.t. R (which does things differently)
(defmethod names ((robj array))
  (first (dimension-names robj)))
|#

#|
(defmethod (setf names) (names (robj array))
  (setf (array-names robj) names))
|#

(defmethod (setf names) (names (a array))
  (let ((anp (array-names-pos a)))
    (and anp (clrhash anp))
    (setf (array-names-pos a)
          (init-names names)
          
          (array-names a)
          names)
    ))


;;; Printing.
;;; =========

(defmethod print-object ((a array) stream)
  (let* ((adata (array-data a))
         (aname (array-name a))
         (anl (cl:length (string aname)))
         )
    (declare (type cl:array a)
             (type (or string symbol) aname)
             (type fixnum anl)
             )

    (labels ((print-array-by-matrixes (a stream
                                         &aux
                                         (dimensions (dimensions a))
                                         (dim-names (dimension-names a))
                                         (dl (cl:length dimensions)))
               (declare (type cl:array a)
                        (type stream stream)
                        (type cl:list dimensions dimension-names)
                        (type fixnum dl)
                        )
               (case dl
                 (0 (print-0-dimensional-array a stream))
                 (1 (print-vector-array a (first dimensions) stream))
                 (2 (print-submatrix-array a () dimensions () dim-names stream))
                 (t (print-subdimensions-array a () dimensions () dim-names stream))
                 ))

             (print-0-dimensional-array (a stream)
               (declare (ignore a)
                        (type stream stream))
               (format stream "~@[~A ~][]" aname))

             (print-submatrix-index (stream submatrix-index submatrix-name-index)
               (declare (type stream stream)
                        (type cl:list submatrix-index submatrix-name-index)
                        )
               (when (or submatrix-index submatrix-name-index)
                 (format stream "~2&<")
                 (loop with smnl = submatrix-name-index
                       for smi in submatrix-index
                       for smnl-sublist = smnl then (rest smnl-sublist)
                       for (smni) = smnl-sublist
                       if smni do (format stream "~A, " smni)
                       else do (format stream "~A, " smi)
                       finally (format stream ", >~%")))

               #|
                      (format stream
                              "~@[~&@~{ ~D~} * *~%~]" submatrix-index)
               |#
               )

             (print-submatrix-array (a
                                     submatrix-index
                                     rxc
                                     submatrix-name-index
                                     dimension-names
                                     stream
                                     &aux
                                     (r (first rxc))
                                     (c (second rxc))
                                     )
               (declare (ignore a)
                        (type cl:list
                              submatrix-index
                              submatrix-name-index
                              dimension-names)
                        (type stream stream)
                        (type fixnum r c)
                        )

               ;; Print the submatrix header.

               (print-submatrix-index stream submatrix-index submatrix-name-index)

               ;; Collect various formatting values by traversing row
               ;; and column headers and data elements.

               (let* ((r-header-format "[~A, ]") ; Or "(@ ~A *)".
                      (c-header-format "[, ~A]") ; Or "(@ * ~A)".
                      (r-headers
                       (loop with r-names = (first dimension-names)
                             for i from 0 below r
                             for rns = r-names then (rest rns)
                             for (r-name) = rns
                             if r-name collect (format nil r-header-format r-name)
                             else collect (format nil r-header-format i)))
                      (c-headers
                       (loop with c-names = (second dimension-names)
                             for j from 0 below c
                             for cns = c-names then (rest cns)
                             for (c-name) = cns
                             if c-name collect (format nil c-header-format c-name)
                             else collect (format nil c-header-format j)))
                      (max-r-header (loop for r-head in r-headers
                                          maximize (cl:length r-head)))
                      (max-c-header (loop for c-head in c-headers
                                          maximize (cl:length c-head)))
                      (elements
                       (loop for i from 0 below r
                             collect
                             (loop for j from 0 below c
                                   collect
                                   (format nil "~A"
                                           (apply #'aref adata
                                                  (append submatrix-index
                                                          (list i j)))))))
                      (max-element
                       (loop for row in elements
                             maximize
                             (loop for e in row
                                   maximize (cl:length e))))
                      (max-el-length (max max-element max-c-header))
                      )
                 (declare (type cl:list
                                r-headers
                                c-headers
                                elements)
                          (type string
                                r-header-format
                                c-header-format)
                          (type fixnum
                                max-r-header
                                max-c-header
                                max-element
                                max-el-length)
                          )

                 ;; Note: come back later to fix limits etc.

                 ;; Print the headers.
                 (format stream "~VT~:{ ~V@A~}~%"
                         max-r-header
                         (mapcar (lambda (c-head)
                                   (list max-el-length c-head))
                                 c-headers))


                 ;; Print the matrix eleemnts.
                 (loop for r-head in r-headers
                       for row in elements
                       do (format stream "~V@A~:{ ~V@A~}~%"
                                  max-r-header
                                  r-head
                                  (mapcar (lambda (e)
                                            (list max-el-length e))
                                          row)))                
                 ))

             (print-subdimensions-array (a subdimension-index
                                           dimensions
                                           subdimension-names
                                           dimension-names
                                           stream)
               (declare (type cl:array a)
                        (type stream stream)
                        (type cl:list
                              subdimension-index
                              subdimension-names
                              dimensions
                              dimension-names)
                        )
               (if (= 2 (cl:length dimensions))
                   (print-submatrix-array a
                                          (nreverse subdimension-index)
                                          dimensions
                                          (nreverse subdimension-names)
                                          dimension-names
                                          stream)
                   (loop with d-names of-type cl:list = (first dimension-names)
                         for k of-type fixnum from 0 below (first dimensions)
                         for d-ns = d-names then (rest d-ns)
                         do (print-subdimensions-array a
                                                       (cons k subdimension-index)
                                                       (rest dimensions)
                                                       (cons (first d-ns) subdimension-names)
                                                       (rest dimension-names)
                                                       stream)))
               )

             (print-vector-array (a size stream)
               (declare (ignore size)
                        (type cl:array a)
                        (type stream stream)
                        )
               (let* ((names (or (and (dimension-names a)
                                      (first (dimension-names a)))
                                 #| (dimension-names a) |#
                                 (array-names a)))
                      (longest-name (loop for n in names
                                          maximize (cl:length (format nil "~A" n))))
                      (vals (loop for v across adata
                                  collect (format nil "~A" v)))
                      (longest-value (loop for vs in vals
                                           maximize (cl:length vs)))
                      (field-max-length (max longest-name longest-value))
                      )
                 (declare (type cl:list names vals)
                          (type fixnum longest-name longest-value field-max-length)
                          )
                 (format stream "~:[~2*~;~VA~%~]~@[~:{  ~V@A~}~%~]~:[[]~;~:*~:{  ~V@A~}~]~2%"
                         aname
                         anl
                         aname
                         (mapcar (lambda (n)
                                   (list field-max-length n))
                                 names)
                         (map 'list (lambda (v)
                                      (list field-max-length v))
                              adata))
                 ))
             ) ; labels
    
      (cond (*print-readably*
             (call-next-method))

            (*print-displaying*
             (print-array-by-matrixes a stream)
             )

            (t
             (print-unreadable-object (a stream :type t)
               (format stream "~A" (array-dimensions (array-data a))))))
      )))


(defmethod as-vector ((v cl:vector)) v)

(defmethod as-vector ((l cl:list))
  (make-array (list-length l) :initial-contents l))

(defmethod as-vector ((a cl:array))
  (make-array (array-total-size a)
              :element-type (cl:array-element-type a)
              :displaced-to a))

(defmethod as-vector ((a array))
  (as-vector (array-data a)))


(defun unlist (a)
  (vector* (as-vector a)))


;;;; end of file -- array.lisp
