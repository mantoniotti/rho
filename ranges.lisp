;;;; -*- Mode: Lisp -*-

;;;; ranges.lisp

(in-package "RHO")

(defstruct (range (:include vector)
                  (:constructor range (&optional
                                       (from 0)
                                       (to 0)
                                       (by 1)
                                       &aux
                                       (data
                                        (fill-range-data from to by))
                                       )))

  (from 0 :type real :read-only t)
  (to 0 :type real :read-only t)
  (by 1 :type real :read-only t)
  )


(defun fill-range-data (rfrom rto rby)
  (declare (type real rfrom rto rby))

  (when (zerop rby)
    (error "Zero step while filling RANGE."))

  (when (and (<= rto rfrom)
             (minusp rby))
    (error "Inconsistent RANGE parameters ~S, ~S, ~S."
           rfrom rto rby))

  (if (<= rto rfrom)
      (loop for i from rfrom above rto by rby
            collect i into result
            finally (return (coerce result 'cl:vector)))
      (loop for i from rfrom below rto by rby
            collect i into result
            finally (return (coerce result 'cl:vector)))))


(defmethod at ((v vector) (r range) &rest more-indexes)
  (declare (ignore more-indexes))
  (at v (range-data r))
  )

;;;; end of file -- ranges.lisp
