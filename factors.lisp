;;;; -*- Mode: Lisp -*-

;;;; factors.lisp

(in-package "RHO")

;;; comparator
;;; Move this elsewhere.

(defgeneric comparator (x))

(defmethod comparator ((s list))
  (comparator (first s)))

(defmethod comparator ((s vector))
  (comparator (aref s 0)))

(defmethod comparator ((s character)) #'char<)
(defmethod comparator ((s string)) #'string<)
(defmethod comparator ((s symbol)) #'string<)




(defstruct (factor (:include vector)
                   (:conc-name %factor-)
                   (:constructor %make-factor)
                   )
  (levels #() :type cl:vector)
  (used-levels #* :type cl:bit-vector)
  )


(defgeneric factor (x
                    &key
                    levels
                    exclude
                    labels
                    ordered
                    nmax
                    &allow-other-keys)
  (:documentation "The FACTOR Constructor."))


(defmethod factor ((els sequence)
                   &key
                   levels
                   exclude
                   labels
                   &allow-other-keys)
  (declare (type sequence levels exclude))
  (let* ((els (coerce els 'cl:vector))

         (actual-levels
          (if levels
              (coerce levels 'list)
              (coerce (remove-duplicates els :test #'equalp) 'list)))

         (factor-levels
          (if exclude
              (set-difference actual-levels
                              (coerce exclude 'list)
                              :test #'equalp)
              actual-levels))

         (f (%make-factor
             :data (make-array (cl:length els)
                               :element-type '(or null fixnum) ; Really INDEX
                               )
             :levels (sort (coerce factor-levels 'cl:vector)
                           (comparator (first factor-levels)))
             :used-levels (make-array (cl:length factor-levels)
                                      :element-type 'bit
                                      :initial-element 0)
             ))
         )
    (declare (type cl:vector els)
             (type list actual-levels factor-levels)
             (type factor f))
    (init-factor-levels els f)))


(defmethod factor ((f factor)
                   &key
                   levels
                   exclude
                   labels
                   &allow-other-keys)
  ;; Always copies.  Never a true no-op.
  (let* ((f-levels (%factor-levels f))
         (f-used-levels (%factor-used-levels f))
         (used-levels (loop for l across f-levels
                            for j from 0
                            when (not (zerop (bit f-used-levels j)))
                            collect l))
         (f-data (loop for i across (%factor-data f)
                       collect (and i (aref f-levels i)) into fds
                       finally (return (coerce fds 'cl:vector))))

         (actual-levels
          (if levels
              (coerce levels 'list)
              used-levels))

         (factor-levels
          (if exclude
              (set-difference actual-levels
                              (coerce exclude 'list)
                              :test #'equalp)
              actual-levels))
         
         (f (%make-factor
             :data (copy-seq f-data)
             :levels (sort (coerce factor-levels 'cl:vector)
                           (comparator (first factor-levels)))
             :used-levels (make-array (cl:length factor-levels)
                                      :element-type 'bit
                                      :initial-element 0)
             ))
         )

    (init-factor-levels f-data f)
    ))


(defun init-factor-levels (els f)
  (declare (type cl:vector els)
           (type factor f))

  (let ((data (%factor-data f))
        (levels (%factor-levels f))
        (used-levels (%factor-used-levels f))
        (lht (make-hash-table :test #'equalp))
        )
    
    (declare (type hash-table lht)
             (dynamic-extent lht)
             (type cl:vector data levels)
             (type cl:bit-vector used-levels))

    (loop for l across levels
          for i from 0
          do (setf (gethash l lht) i))
    (loop for e across els
          for j from 0
          for ej = (gethash e lht)
          do (setf (aref data j) ej)
          when ej do (setf (bit used-levels ej) 1))
    f))


(defmethod print-object ((f factor) stream)
  (if *print-readably*
      (call-next-method)
      (format stream
              "Items:  ~S~%~
               Levels: ~S~%"

              (loop for i across (%factor-data f)
                    collect (and i (aref (%factor-levels f) i)))
                    
              (coerce (%factor-levels f) 'list))))



            
    






(defgeneric code (x)
  (:method ((x integer)) x)
  (:method ((y real)) (round y))
  (:method ((c character)) (char-code c))
  )




(defgeneric tabulate (v &optional nbins))

(defmethod tabulate :before ((v sequence) &optional nbins)
  (unless (every (complement #'minusp) v)
    (let* ((nn (find-if #'minusp v))
           (nnpos (position nn v))
           )
      (error "Argument contains negative numbers: ~S in position ~D."
             nn
             nnpos))))


(defmethod tabulate ((v list)
                     &optional
                     (nbins (1+ (loop for x in v maximize x))))
  (let ((r (make-array nbins :element-type 'fixnum :initial-element 0)))
    (dolist (x v r)
      (when (< x nbins)
        (incf (aref r (code x)))))))


(defmethod tabulate ((v cl:vector)
                     &optional
                     (nbins (1+ (loop for x across v maximize x))))
  (let ((r (make-array nbins :element-type 'fixnum :initial-element 0)))
    (loop for x across v
          when (< x nbins) do (incf (aref r (code x)))
          finally (return r))))


;;;; end of file -- factors.lisp
