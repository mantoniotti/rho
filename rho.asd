;;;; -*- Mode: Lisp -*-

;;;; rho.asd --

;;;; See the file COPYING for licencing and copyright information.

(asdf:defsystem :rho
  :author "Marco Antoniotti"
  :license "BSD"
  :components ((:file "rho-package")
               (:file "printing" :depends-on ("rho-package"))
               (:file "types-utils" :depends-on ("rho-package"))
               (:file "robj" :depends-on ("rho-package" "types-utils" "printing"))
               (:file "indexing" :depends-on ("robj"))
               (:file "shape" :depends-on ("rho-package"))
               (:file "array" :depends-on ("shape" "robj" "indexing"))
               (:file "matrix" :depends-on ("array" "vector"))
               (:file "vector" :depends-on ("array"))
               (:file "array-predicates"
                :depends-on ("array" "vector" "matrix"))
               (:file "array-indexing"
                :depends-on ("array" "vector" "matrix" "array-predicates"))
               (:file "ranges" :depends-on ("vector"))
               (:file "factors" :depends-on ("vector"))
               (:file "shape-robj"
                :depends-on ("array"
                             "vector"
                             "matrix"
                             "ranges"
                             "factors"
                             "array-indexing"
                             ))
               ;; (:file "data-frame" :depends-on ("rho-package"))
               )
  :description
  "The RHO System.

The RHO System contains an implementation of a numebr of R-like
facilities and data structures on top of Common Lisp."
  )


;;;; end of file -- rho.asd --
